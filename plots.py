# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 11:24:43 2023

@author: Matthias Maldet
"""

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from matplotlib import cm
from matplotlib.ticker import LinearLocator
import plotly.offline as pyo
import os



def get_electricity_sankey(*args, **kwargs):
    
    
    data_timeseries= kwargs.get("results_timeseries", False)
    data_eeg= kwargs.get("results_eeg", False)
    energy_community= kwargs.get("energy_community", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    consumer_target = consumers
    
    
    
    if not os.path.exists(scenario + "/balance_electricity"):
        os.makedirs(scenario + "/balance_electricity")
    
    for consumer in consumers:
        
        source=[]
        target=[]
        values=[]
        labels=[]
        color=[]
        
        
        # ------------- Inputs --------------
        #Electricity grid purchase
        source.append(0)
        target.append(1)
        values.append(np.sum(data_timeseries["q_elgrid"][consumer].values))
        labels.append("q_elgrid_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["q_elgrid"][consumer].values))) + "kWh")
        color.append("red")
        
        labels.append("")
        color.append("black")
        
        #PV
        source.append(2)
        target.append(1)
        values.append(np.sum(data_timeseries["q_pv"][consumer].values))
        labels.append("q_pv_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_pv"][consumer].values))) + "kWh")
        color.append("green")
        
        #Battery discharge
        source.append(3)
        target.append(1)
        values.append(np.sum(data_timeseries["q_batdischarge"][consumer].values))
        labels.append("q_batdischarge__" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_batdischarge"][consumer].values))) + "kWh")
        color.append("blue")
        
        
        
        # -------------- Outputs ---------------------------
        #Grid feedin
        source.append(1)
        target.append(4)
        values.append(np.sum(data_timeseries["q_feedin"][consumer].values))
        labels.append("q_feedin_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_feedin"][consumer].values)))+ "kWh")
        color.append("red")
        
        #Electricity demand
        source.append(1)
        target.append(5)
        values.append(np.sum(data_timeseries["q_eldemand"][consumer].values))
        labels.append("q_eldemand_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_eldemand"][consumer].values)))+ "kWh")
        color.append("yellow")
        
        #Battery charge
        source.append(1)
        target.append(6)
        values.append(np.sum(data_timeseries["q_batcharge"][consumer].values))
        labels.append("q_batcharge_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_batcharge"][consumer].values)))+ "kWh")
        color.append("blue")
        
        #Heat pump electricity
        source.append(1)
        target.append(7)
        values.append(np.sum(data_timeseries["q_HPel"][consumer].values))
        labels.append("q_HPel_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_HPel"][consumer].values)))+ "kWh")
        color.append("brown")
        
        #Electric cooling
        source.append(1)
        target.append(8)
        values.append(np.sum(data_timeseries["q_elCoolel"][consumer].values))
        labels.append("q_elCoolel_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_elCoolel"][consumer].values)))+ "kWh")
        color.append("darkblue")
        
        # -------------Community processes
        
        if(energy_community):
            i=9
            for consumer1 in consumer_target:
                if(consumer in consumer1):
                    continue
                
                #Purchase from consumers
                source.append(i)
                target.append(1)
                values.append(np.sum(data_eeg[('q_c2L', consumer, consumer1)].values))
                labels.append("q_c2L_" + str(consumer).split("_")[1] + "," +   str(consumer1).split("_")[1]  + ": " + str(round(np.sum(data_eeg[('q_c2L', consumer, consumer1)].values)))+ "kWh")
                color.append("lightseagreen")    
                
                i+=1
                
                #Sale to consumers
                source.append(1)
                target.append(i)
                values.append(np.sum(data_eeg[('q_L2c', consumer, consumer1)].values))
                labels.append("q_L2c_" + str(consumer).split("_")[1] + "," +   str(consumer1).split("_")[1]  + ": " + str(round(np.sum(data_eeg[('q_L2c', consumer, consumer1)].values)))+ "kWh")
                color.append("lightseagreen")    
                
    
                i+=1


        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = labels,
              color=color
            ),
            link = dict(
              source = source,
              target = target,
              value = values,
          ))])

        

        fig.update_layout(title_text="Electricity " + str(consumer), font_size=10)
        filename = scenario + "/balance_electricity" + "/electricity_" + str(consumer) + ".png"
        fig.show()
        fig.write_image(filename, scale=3)
        
        
def get_heat_sankey(*args, **kwargs):
    
    
    data_timeseries= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")

    
    
    
    if not os.path.exists(scenario + "/balance_heat"):
        os.makedirs(scenario + "/balance_heat")
    
    for consumer in consumers:
        
        source=[]
        target=[]
        values=[]
        labels=[]
        color=[]
        
        # ------------- Inputs --------------
        #Heat pump input
        source.append(0)
        target.append(1)
        values.append(np.sum(data_timeseries["q_HPth"][consumer].values))
        labels.append("q_HPth_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["q_HPth"][consumer].values))) + "kWh")
        color.append("brown")
        
        labels.append("")
        color.append("black")
        
        #Heat storage discharge
        source.append(2)
        target.append(1)
        values.append(np.sum(data_timeseries["q_thstoredischarge"][consumer].values))
        labels.append("q_thstoredischarge_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_thstoredischarge"][consumer].values))) + "kWh")
        color.append("blue")
        
        #District heat
        source.append(3)
        target.append(1)
        values.append(np.sum(data_timeseries["q_dh"][consumer].values))
        labels.append("q_dh_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_dh"][consumer].values))) + "kWh")
        color.append("red")
        
        
        
        # -------------- Outputs ---------------------------
        #Thermal storage charge
        source.append(1)
        target.append(4)
        values.append(np.sum(data_timeseries["q_thstorecharge"][consumer].values))
        labels.append("q_thstorecharge_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_thstorecharge"][consumer].values)))+ "kWh")
        color.append("blue")
        
        #Heat demand
        source.append(1)
        target.append(5)
        values.append(np.sum(data_timeseries["q_heatdemand"][consumer].values))
        labels.append("q_heatdemand_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_heatdemand"][consumer].values)))+ "kWh")
        color.append("yellow")
        
        


        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = labels,
              color=color
            ),
            link = dict(
              source = source,
              target = target,
              value = values,
          ))])

        

        fig.update_layout(title_text="Heat " + str(consumer), font_size=10)
        filename = scenario + "/balance_heat" + "/heat_" + str(consumer) + ".png"
        fig.show()
        fig.write_image(filename, scale=3)
        
        
def get_cooling_sankey(*args, **kwargs):
    
    
    data_timeseries= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")

    
    
    
    if not os.path.exists(scenario + "/balance_cooling"):
        os.makedirs(scenario + "/balance_cooling")
    
    for consumer in consumers:
        
        source=[]
        target=[]
        values=[]
        labels=[]
        color=[]
        
        # ------------- Inputs --------------
        #Electric cooling
        source.append(0)
        target.append(1)
        values.append(np.sum(data_timeseries["q_elCoolcool"][consumer].values))
        labels.append("q_elCoolcool_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["q_elCoolcool"][consumer].values))) + "kWh")
        color.append("brown")
        
        labels.append("")
        color.append("black")
        
        
        #District cooling
        source.append(2)
        target.append(1)
        values.append(np.sum(data_timeseries["q_dcool"][consumer].values))
        labels.append("q_dcool_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_dcool"][consumer].values))) + "kWh")
        color.append("red")
        
        
        
        # -------------- Outputs ---------------------------

        #Cooling demand
        source.append(1)
        target.append(3)
        values.append(np.sum(data_timeseries["q_cooldemand"][consumer].values))
        labels.append("q_cooldemand_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_cooldemand"][consumer].values)))+ "kWh")
        color.append("yellow")
        
        


        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = labels,
              color=color
            ),
            link = dict(
              source = source,
              target = target,
              value = values,
          ))])

        

        fig.update_layout(title_text="Cooling " + str(consumer), font_size=10)
        filename = scenario + "/balance_cooling" + "/cooling_" + str(consumer) + ".png"
        fig.show()
        fig.write_image(filename, scale=3)
        
        
def get_emission_sankey(*args, **kwargs):
    
    
    data_timeseries= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")

    
    
    
    if not os.path.exists(scenario + "/balance_emissions"):
        os.makedirs(scenario + "/balance_emissions")
    
    for consumer in consumers:
        
        source=[]
        target=[]
        values=[]
        labels=[]
        color=[]
        
        # ------------- Inputs --------------
        #Electricity grid emissions
        source.append(0)
        target.append(1)
        values.append(np.sum(data_timeseries["em_elgrid"][consumer].values))
        labels.append("em_elgrid_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["em_elgrid"][consumer].values))) + "kgCO2")
        color.append("red")
        
        labels.append("")
        color.append("black")
        
        
        #District heat grid emissions
        source.append(2)
        target.append(1)
        values.append(np.sum(data_timeseries["em_dh"][consumer].values))
        labels.append("em_dh_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["em_dh"][consumer].values))) + "kgCO2")
        color.append("green")
        
        #District cooling grid emissions
        source.append(3)
        target.append(1)
        values.append(np.sum(data_timeseries["em_dcool"][consumer].values))
        labels.append("em_dcool_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["em_dcool"][consumer].values))) + "kgCO2")
        color.append("blue")
        
        
        # -------------- Outputs ---------------------------
        #Total emissions
        source.append(1)
        target.append(4)
        values.append(np.sum(data_timeseries["em_total"][consumer].values))
        labels.append("em_total_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["em_total"][consumer].values)))+ "kgCO2")
        color.append("brown")
        
        
        


        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = labels,
              color=color
            ),
            link = dict(
              source = source,
              target = target,
              value = values,
          ))])

        

        fig.update_layout(title_text="Emissions " + str(consumer), font_size=10)
        filename = scenario + "/balance_emissions" + "/emissions_" + str(consumer) + ".png"
        fig.show()
        fig.write_image(filename, scale=3)
        
        
        
def get_community_heatmaps(*args, **kwargs):
    
    
    
    data_eeg= kwargs.get("results_eeg", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/trading"):
        os.makedirs(scenario + "/trading")
        
        
    df_c2L=pd.DataFrame(index=consumers)
    
    df_L2c=pd.DataFrame(index=consumers)
    
    consumers1=consumers
    
    for consumer in consumers:
        
        values_c2L=[]
        
        values_L2c=[]
        
        for consumer1 in consumers1:
            values_c2L.append(np.sum(data_eeg[('q_c2L', consumer, consumer1)].values))
            values_L2c.append(np.sum(data_eeg[('q_L2c', consumer, consumer1)].values))
            
        df_c2L[consumer] = values_c2L
        df_L2c[consumer] = values_L2c
        
    #Heat map c2L
    ax = sns.heatmap(df_c2L, linewidths=2, square=True, cmap='Blues', cbar_kws={'label': 'Traded electricity in kWh'}, annot=True, fmt='.0f')
    ax.set_title('Community purchase', fontsize = 12)
    plt.show()
    fig=ax.get_figure()
    fig.set_dpi(600)
    fig.set_size_inches(18.5, 10.5)
    filename = scenario + "/trading" + "/communitypurchase.png"
    fig.savefig(filename) 
    
    #Heat map cL2c
    ax = sns.heatmap(df_L2c, linewidths=2, square=True, cmap='Blues', cbar_kws={'label': 'Traded electricity in kWh'}, annot=True, fmt='.0f')
    ax.set_title('Community sale', fontsize = 12)
    plt.show()
    fig=ax.get_figure()
    fig.set_dpi(600)
    fig.set_size_inches(18.5, 10.5)
    filename = scenario + "/trading" + "/communitysale.png"
    fig.savefig(filename) 
    
    
def get_peakpower_bar(*args, **kwargs):
    
    data_peakpower= kwargs.get("results_peakpower", False)
    scenario= kwargs.get("scenario", "Test")
    aggregated = kwargs.get("aggregate_peakpower", False)
    
    if not os.path.exists(scenario + "/peakpower"):
        os.makedirs(scenario + "/peakpower")
        
    ind = data_peakpower["p_max"].index.values
    ind=ind[:-1]
    
    
    ind = np.append(ind, "Aggregated")
        
    peakpower = data_peakpower["p_max"].values
    peakpower=peakpower[:-1]
        
    if(aggregated):
        peak_agg=data_peakpower["p_max_agg"].values[-1][0]
        peakpower=np.append(peakpower, peak_agg)
    else:
        peakpower=np.append(peakpower, 0)

    
    #Plot bar chart
    fig = plt.figure()
    fig.set_dpi(600)
    ax = fig.add_axes([0,0,1,1])
    ax.set_ylabel('Peak power in kW')
    ax.bar(ind, peakpower)
    #ax.legend(labels=[cols[0], cols[4]])
    plt.xticks(size = 8)
    filename = scenario + "/peakpower" + "/peakpower.png"
    fig.savefig(filename, bbox_inches='tight') 
    
    
    
def get_electricitygrid_combined_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_elgrid_combined"):
        os.makedirs(scenario + "/loadcurve_elgrid_combined")
        
    for consumer in consumers:
        df=data["q_elgrid"][consumer]
        df_feedin=data["q_feedin"][consumer]
        
        x=np.append(df.values, -df_feedin.values)
        ind=np.arange(len(x))
        
        plt.figure(dpi=600)
        plt.plot(ind, sorted(x, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('Electricity grid load in kWh')
        plt.title('Load curve electricity grid ' + consumer)
        filename = scenario + "/loadcurve_elgrid_combined" + "/elgrid_combined_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
    
def get_electricitygrid_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_elgrid"):
        os.makedirs(scenario + "/loadcurve_elgrid")
        
    for consumer in consumers:
        df=data["q_elgrid"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('Electricity grid consumption in kWh')
        plt.title('Load curve electricity grid ' + consumer)
        filename = scenario + "/loadcurve_elgrid" + "/elgrid_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
def get_electricityfeedin_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_elgridfeedin"):
        os.makedirs(scenario + "/loadcurve_elgridfeedin")
        
    for consumer in consumers:
        df=data["q_feedin"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('Electricity grid feedin in kWh')
        plt.title('Load curve electricity grid feedin ' + consumer)
        filename = scenario + "/loadcurve_elgridfeedin" + "/elgridfeedin_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
        
def get_districtheat_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_heat"):
        os.makedirs(scenario + "/loadcurve_heat")
        
    for consumer in consumers:
        df=data["q_dh"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('District heat purchase in kWh')
        plt.title('Load curve district heat purchase ' + consumer)
        filename = scenario + "/loadcurve_heat" + "/districtheat_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
def get_districtcool_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_cool"):
        os.makedirs(scenario + "/loadcurve_cool")
        
    for consumer in consumers:
        df=data["q_dcool"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('District cooling purchase in kWh')
        plt.title('Load curve district cooling purchase ' + consumer)
        filename = scenario + "/loadcurve_cool" + "/districtcooling_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()

def get_emission_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_emissions"):
        os.makedirs(scenario + "/loadcurve_emissions")
        
    for consumer in consumers:
        df=data["em_total"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('Total emissions in kgCO2')
        plt.title('Load curve emissions ' + consumer)
        filename = scenario + "/loadcurve_emissions" + "/emissions_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
        
def get_battery_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/battery"):
        os.makedirs(scenario + "/battery")
        
    for consumer in consumers:
        df_SOC=data["q_batSOC"][consumer]
        df_charge=data["q_batcharge"][consumer]
        df_discharge=data["q_batdischarge"][consumer]
        
        fig, axs = plt.subplots(3, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Battery ' + consumer, fontsize = 18)
        axs[0].plot(df_SOC.index.values, df_SOC.values)
        axs[0].set_ylabel('Battery SOC in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_SOC.index.values, df_charge.values)
        axs[1].set_ylabel('Battery charging kWh', color='k', fontsize=14)
        axs[1].xaxis.label.set_visible(False)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        axs[2].plot(df_SOC.index.values, [-x for x in df_discharge.values])
        axs[2].set_ylabel('Battery discharging kWh', color='k', fontsize=14)
        axs[2].set_xlabel('Time in h', color='k', fontsize=14)
        axs[2].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/battery" + "/battery_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        

def get_thstore_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/thstore"):
        os.makedirs(scenario + "/thstore")
        
    for consumer in consumers:
        df_SOC=data["q_thstoreSOC"][consumer]
        df_charge=data["q_thstorecharge"][consumer]
        df_discharge=data["q_thstoredischarge"][consumer]
        
        fig, axs = plt.subplots(3, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Thermal storage ' + consumer, fontsize = 18)
        axs[0].plot(df_SOC.index.values, df_SOC.values, 'red')
        axs[0].set_ylabel('ThStore SOC in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_SOC.index.values, df_charge.values, 'red')
        axs[1].set_ylabel('ThStore charging kWh', color='k', fontsize=14)
        axs[1].xaxis.label.set_visible(False)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        axs[2].plot(df_SOC.index.values,[-x for x in df_discharge.values], 'red')
        axs[2].set_ylabel('ThStore discharging kWh', color='k', fontsize=14)
        axs[2].set_xlabel('Time in h', color='k', fontsize=14)
        axs[2].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/thstore" + "/thstore_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
def get_costs_peakpower_bar(*args, **kwargs):
    
    costs_peakpower= kwargs.get("costs_power", False)
    scenario= kwargs.get("scenario", "Test")
    consumers= kwargs.get("consumers", False)
    
    consumers.append("Aggregated")

    
    if not os.path.exists(scenario + "/costs_peakpower"):
        os.makedirs(scenario + "/costs_peakpower")
        

    #Plot bar chart
    fig = plt.figure()
    fig.set_dpi(600)
    ax = fig.add_axes([0,0,1,1])
    ax.set_ylabel('Peak power costs in €')
    ax.bar(consumers, costs_peakpower.values[0])
    plt.xticks(size = 8)
    filename = scenario + "/costs_peakpower" + "/costs_peakpower.png"
    fig.savefig(filename, bbox_inches='tight') 
    
    consumers.remove("Aggregated")
    
    
def get_costs_community_bar(*args, **kwargs):
    
    costs_eeg= kwargs.get("costs_eeg", False)
    scenario= kwargs.get("scenario", "Test")
    consumers= kwargs.get("consumers", False)
    
    consumers1=consumers
    
    if not os.path.exists(scenario + "/costs_community"):
        os.makedirs(scenario + "/costs_community")

    for consumer in consumers:
        
        buy=[]
        sale=[]
        lbl_buy=[]
        lbl_sale=[]
        
        for consumer1 in consumers1:
            
            if consumer==consumer1:
                continue
            
            buy.append(np.sum(costs_eeg[("q_c2L", consumer, consumer1)].values))
            sale.append(np.sum(costs_eeg[("q_L2c", consumer, consumer1)].values))
            lbl_buy.append("Buy_(" + consumer.split('_')[1] + "," + consumer1.split('_')[1] + ")")
            lbl_sale.append("Sale_(" + consumer.split('_')[1] + "," + consumer1.split('_')[1] + ")")
            
            
        #Plot bar chart
        fig = plt.figure()
        fig.set_dpi(600)
        ax = fig.add_axes([0,0,1,1])
        plt.axhline(0, color='black', linewidth=1)
        ax.set_ylabel('Community costs ' + consumer + ' in €')  
        ax.bar(np.append(lbl_buy, lbl_sale), np.append(buy, sale))
        plt.xticks(size = 8)
        filename = scenario + "/costs_community" + "/costs_community_" + consumer + ".png"
        fig.savefig(filename, bbox_inches='tight') 
            
            
    
    
def get_costs_total(*args, **kwargs):
    
    costs_timeseries= kwargs.get("costs_timeseries", False)
    costs_eeg= kwargs.get("costs_eeg", False)
    costs_power= kwargs.get("costs_power", False)
    scenario= kwargs.get("scenario", "Test")
    consumers= kwargs.get("consumers", False)
    
    consumers1=consumers
    
    if not os.path.exists(scenario + "/costs_total"):
        os.makedirs(scenario + "/costs_total")
        
        
    
    tot_labels=[]
    tot_values=[]
    
    for consumer in consumers:
        
        consumer_labels=[]
        consumer_values=[]
        
        # ----- Costs timeseries ----------
        
        #Iterate over all values
        for col in costs_timeseries.columns:
            
            #If consumer in column, save the value
            if(str(consumer) in str(col)):
                
                data=np.sum(costs_timeseries[col])
                
                #But only if inequal to zero
                if(data!=0):
                    if("em_total" in str(col)):
                        consumer_labels.append("em")
                    elif("thstorecharge" in str(col)):
                        consumer_labels.append("thcharge")
                    elif("thstoredischarge" in str(col)):
                        consumer_labels.append("thdischarge")
                    else:
                        consumer_labels.append(col[0].split('_')[1])
                    consumer_values.append(data)
                    
                    
        # ----- Costs eeg --------
        
        buy=[]
        sale=[]
        for consumer1 in consumers1:
           
            if(consumer1 != consumer):
                buy.append(np.sum(costs_eeg[("q_c2L", consumer, consumer1)].values))
                sale.append(np.sum(costs_eeg[("q_L2c", consumer, consumer1)].values))
           
           
        #Append community costs to consumers
        consumer_labels.append("Com Purchase")
        consumer_values.append(np.sum(buy))
        consumer_labels.append("Com Sale")
        consumer_values.append(np.sum(sale))
        
        
        # ------ Costs peakpower -----------
        
        c_power = costs_power[("p_max", consumer)].values[0]
        
        if(c_power!=0):
            consumer_labels.append("P_peak")
            consumer_values.append(c_power)
        
           
        #Append total consumer costs            
        consumer_labels.append("Total")
        total=np.sum(consumer_values)
        consumer_values.append(total)
        
        tot_labels.append(consumer)
        tot_values.append(total)
        
        #Plot bar chart
        fig = plt.figure()
        fig.set_dpi(600)
        fig.suptitle('Costs ' + consumer, fontsize = 12)
        ax = fig.add_axes([0,0,1,1])
        plt.axhline(0, color='black', linewidth=1)
        ax.set_ylabel('Costs in €')  
        ax.bar(consumer_labels, consumer_values)
        plt.xticks(size = 5)
        filename = scenario + "/costs_total" + "/costs_total_" + consumer + ".png"
        fig.savefig(filename, bbox_inches='tight') 
        
        
    #Sum total values
    tot_labels.append("Total")
    tot_values.append(np.sum(tot_values))
    
    
    #Plot bar chart total
    fig = plt.figure()
    fig.set_dpi(600)
    fig.suptitle('Total costs', fontsize = 12)
    ax = fig.add_axes([0,0,1,1])
    plt.axhline(0, color='black', linewidth=1)
    ax.set_ylabel('Costs in €')  
    ax.bar(tot_labels, tot_values)
    plt.xticks(size = 8)
    filename = scenario + "/costs_total" + "/costs_total.png"
    fig.savefig(filename, bbox_inches='tight') 
        
      
def get_KPI(*args, **kwargs):
    
    costs_objective= kwargs.get("costs_objective", False)
    scenario= kwargs.get("scenario", "Test")
    consumers= kwargs.get("consumers", False)
    results_timeseries= kwargs.get("results_timeseries", False)
    results_peakpower= kwargs.get("results_peakpower", False)
    results_eeg= kwargs.get("results_eeg", False)
    aggregate_peakpower= kwargs.get("aggregate_peakpower", False)
    energy_community= kwargs.get("energy_community", False)
    
    consumers1=consumers
    
    if not os.path.exists(scenario + "/KPI"):
        os.makedirs(scenario + "/KPI")
        
    #KPI list
    kpi_lbl=[]
    kpi_val=[]

    #Total costs
    kpi_lbl.append("Total costs in €")
    kpi_val.append(costs_objective.values[0][0])
    
    emissions=[]
    for consumer in consumers:
        
        #Add emissions of consumers to list
        emissions.append(np.sum(results_timeseries["em_total"][consumer].values))
    
    #Append emissions to struct
    kpi_lbl.append("Total emissions in kg")
    kpi_val.append(np.sum(emissions))
    
    #Peak power KPI
    kpi_lbl.append("Electricity grid peak power in kW")
    if aggregate_peakpower:
        kpi_val.append(results_peakpower["p_max_agg"].values[-1][0])
    else:
        kpi_val.append(np.sum(np.nan_to_num(results_peakpower["p_max"].values)))
        
        
    #Electricity grid procurement and feedin, district heating and district cooling grid
    elgrid=[]
    feedin=[]
    dhgrid=[]
    dcool=[]
    for consumer in consumers:
        
        #Add emissions of consumers to list
        elgrid.append(np.sum(results_timeseries["q_elgrid"][consumer].values))
        feedin.append(np.sum(results_timeseries["q_feedin"][consumer].values))
        dhgrid.append(np.sum(results_timeseries["q_dh"][consumer].values))
        dcool.append(np.sum(results_timeseries["q_dcool"][consumer].values))
    
    #Append values to struct
    kpi_lbl.append("Electricity grid consumption in kWh")
    kpi_val.append(np.sum(elgrid))
    
    kpi_lbl.append("Electricity grid feedin in kWh")
    kpi_val.append(np.sum(feedin))
    
    kpi_lbl.append("District heat consumption in kWh")
    kpi_val.append(np.sum(dhgrid))
    
    kpi_lbl.append("District cooling consumption in kWh")
    kpi_val.append(np.sum(dcool))
    
    
    #Community trading
    values_c2L=[]
    if energy_community:
        for consumer in consumers:
            for consumer1 in consumers1:
                values_c2L.append(np.sum(results_eeg[('q_c2L', consumer, consumer1)].values))

    kpi_lbl.append("Traded electricity in kWh")
    kpi_val.append(np.sum(values_c2L))
    
    #Plot the KPIs
    fig, axes = plt.subplots(4, 2, figsize=(10, 12))

    # Plot on each subplot
    k=0
    for i in range(4):
        for j in range(2):
            ax = axes[i, j]
            ax.bar([kpi_lbl[k]], [kpi_val[k]])
            ax.set_xlabel('KPI')
            ax.set_ylabel(round(kpi_val[k], 2))
            k+=1
    
    # Adjust spacing between subplots
    plt.tight_layout()
    
    # Display the plot
    plt.show()
    
    #Save the plot
    filename = scenario + "/KPI" + "/KPI.png"
    fig.savefig(filename, bbox_inches='tight') 
    
    
    #Write KPI results to excel
    df=pd.DataFrame(index=kpi_lbl)
    df["Values"] = kpi_val
    filename = scenario + "/KPI" + "/KPI.xlsx"
    with pd.ExcelWriter(filename, engine='xlsxwriter') as writer:
        sheet_name = 'KPI'
        df.to_excel(writer, sheet_name=sheet_name, index=True)
        
        
        
def get_eldemandshift_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/eldemandshift"):
        os.makedirs(scenario + "/eldemandshift")
        
    for consumer in consumers:
        df_elshiftup=data["q_elshiftup"][consumer]
        df_elshiftdown=data["q_elshiftdown"][consumer]
        
        fig, axs = plt.subplots(2, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Flexible electricity load ' + consumer, fontsize = 18)
        axs[0].plot(df_elshiftup.index.values, df_elshiftup.values, 'red')
        axs[0].set_ylabel('Load increase in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_elshiftdown.index.values,[-x for x in df_elshiftdown.values], 'red')
        axs[1].set_ylabel('Load decrease in kWh', color='k', fontsize=14)
        axs[1].set_xlabel('Time in h', color='k', fontsize=14)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/eldemandshift" + "/eldemandshift_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
        
def get_heatdemandshift_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/heatdemandshift"):
        os.makedirs(scenario + "/heatdemandshift")
        
    for consumer in consumers:
        df_heatshiftup=data["q_heatshiftup"][consumer]
        df_heatshiftdown=data["q_heatshiftdown"][consumer]
        
        fig, axs = plt.subplots(2, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Flexible heat load ' + consumer, fontsize = 18)
        axs[0].plot(df_heatshiftup.index.values, df_heatshiftup.values, 'green')
        axs[0].set_ylabel('Load increase in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_heatshiftdown.index.values,[-x for x in df_heatshiftdown.values], 'green')
        axs[1].set_ylabel('Load decrease in kWh', color='k', fontsize=14)
        axs[1].set_xlabel('Time in h', color='k', fontsize=14)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/heatdemandshift" + "/heatdemandshift_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
        
def get_cooldemandshift_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/cooldemandshift"):
        os.makedirs(scenario + "/cooldemandshift")
        
    for consumer in consumers:
        df_coolshiftup=data["q_coolshiftup"][consumer]
        df_coolshiftdown=data["q_coolshiftdown"][consumer]
        
        fig, axs = plt.subplots(2, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Flexible cooling load ' + consumer, fontsize = 18)
        axs[0].plot(df_coolshiftup.index.values, df_coolshiftup.values, 'blue')
        axs[0].set_ylabel('Load increase in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_coolshiftdown.index.values,[-x for x in df_coolshiftdown.values], 'blue')
        axs[1].set_ylabel('Load decrease in kWh', color='k', fontsize=14)
        axs[1].set_xlabel('Time in h', color='k', fontsize=14)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/cooldemandshift" + "/cooldemandshift_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
def get_demandshift_bar(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/demandshift"):
        os.makedirs(scenario + "/demandshift")
        
    for consumer in consumers:
        df_elshiftup=data["q_elshiftup"][consumer]
        df_heatshiftup=data["q_heatshiftup"][consumer]
        df_coolshiftup=data["q_coolshiftup"][consumer]
        
        elec=np.sum(df_elshiftup.values)
        heat=np.sum(df_heatshiftup.values)
        cool=np.sum(df_coolshiftup.values)

        
        #Plot bar chart
        fig = plt.figure()
        fig.set_dpi(600)
        ax = fig.add_axes([0,0,1,1])
        ax.set_ylabel('Demand shift in kW')
        ax.bar(["Electricity", "Heating", "Cooling"], [elec, heat, cool], color=["red", "green", "blue"])
        plt.xticks(size = 8)
        filename = scenario + "/demandshift" + "/costs_peakpower_" + consumer + ".png"
        fig.savefig(filename, bbox_inches='tight') 