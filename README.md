# RENVELOPE_Model

This model provides the flexibility options for grid impact, economical and environmental analyses for the RENVELOPE. This includes functionalities for renewable energy community (EEG) analyses. The model is established as a linear operational optimization problem in Pyomo.

To perform the optimization, all input data and scenario settings, including EEG input data, must be set in the corresponding excel input files.

Results and plots are automatically generated with the model. Without any alterations, only the excel files must be changed.

<img src="/Setup.png" alt="Alt text" title="Optional title">


## Table of contents

[Model workflow](#model-workflow)

[Scenario setting](#scenario-setting)

[Input data structure](#input-data-structure)

[EEG data structure](#eeg-data-structure)

[Load input functions](#load-input-functions)

[Sets](#sets)

[Variables](#variables)

[Constraints](#constraints)

[Objective function](#objective-function)

[Model solving](#model-solving)

[Save results](#save-results)

[Save costs](#save-costs)

[Save results and costs in Excel files](#save-results-and-costs-in-excel-files)

[Plot results and costs](#plot-results-and-costs)



## Model workflow

The model workflow is performed in several steps:

### Step 1

The model workflow requires the loading of input data from the provided excel file [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx). The scenario seeting is also included in the [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx).

### Step 2

The defined input data is loaded into the model in the code in the [model](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/model.py) and with [support functions](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/load_data.py).

### Step 3

Model variables, constraints and the objective function are defined in [model](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/model.py). Alterations and extensions can be made for different model applications.

### Step 4

The model optimization is performed by running the code in the [model](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/model.py).

### Step 5

The results are saved with functionalities in [support functions](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/load_data.py) and plotted with plot functions in [plots](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/plots.py).

### Model workflow summary

<img src="/workflow.png" alt="Alt text" title="Optional title">

## Scenario setting
The scenario settings are directly defined in the [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx) in the sheet Scenario.

The following parameters are included in the sheet.

| **Parameter** | **Description** | **Description** |
| ------ | ------ | ------ |
| **Index** |   Index for the scenario data. | No adaptation is required |
| **Scenario** |  Name for the scenario - required for result saving | Adaptation of the name |
| **Energy_community** |  Parameter that defines if energy communities are considered | **True** for considered, **False** for not considered |
| **Aggregate_Peakpower** |  Considered power price for aggregated consumers or single consumers | **True** for aggregated, **False** for not aggregated |
| **Test** |  Define a test run scenario with only one timestep | **True** for test run, **False** for run with all timesteps |
| **Resolution** |  Resolution of the timesteps | Value of resolution (e.g., 0.25 for quarter, 1 for hourly resolution) |


## Input data structure

The input data represent the technical configuration for the simulation scenario settings. Additionally, costs must be provided that are considered in the simulation.

The input data must be changed in the excel file [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx). Data for each consumer to be considered must be altered in the corresponding sheet. Addtional consumers can be added by adding an additional sheet in the form **Consumer_i**, where i is the running index of considered consumers. All data must be provided for additional consumers. The following table provides an overview of the data that must be set. It is differed between required time series and single data as inputs.

| **Parameter** | **Description** | **Timeseries/Value** |
| ------ | ------ | ------ |
| **D_el** |   Electricity demand | Timeseries |
| **P_elgrid** |   Electricity grid power | Value |
| **C_elenergy** |   Electricity grid energy price | Timeseries |
| **C_elgrid** |   Electricity grid costs (grid tariff) | Value |
| **C_elpower** |   Electricity grid power tariff | Value |
| **C_Feedin** |   Electricity grid feedin tariff | Timeseries |
| **P_pv** |   PV peak capacity | Value |
| **PV** |   PV geneation | Timeseries |
| **P_bat** |   Battery power | Value |
| **SOC_bat** |   Maximum SOC battery | Value |
| **Eta_bat** |   Charging and discharging efficiency battery | Value |
| **Eta_batSB** |   Standby efficiency battery | Value |
| **C_bat** |   Operational costs battery | Value |
| **D_th** |   Heat demand | Timeseries |
| **P_hp** |  Heat pump power | Value |
| **COP** |   Heat pump COP | Timeseries |
| **C_hp** |   Operational costs heat pump | Value |
| **P_districtheat** |   District heat grid power | Value |
| **C_districtheat** |   District heat procurement costs | Timeseries |
| **P_thstore** |   Thermal storage power | Value |
| **SOC_thstore** |   Maximum SOC thermal storage | Value |
| **Eta_thstore** |   Charging and discharging efficiency thermal storage | Value |
| **Eta_thstoreSB** |   Standby efficiency thermal storage | Value |
| **C_thstore** |   Operational costs thermal storage | Value |
| **D_cool** |   Cooling demand | Timeseries |
| **P_thcool** |  Electric cooling power | Value |
| **COP_cool** |   Electric cooling COP | Timeseries |
| **C_elcool** |   Operational costs electric cooling | Value |
| **P_districtcool** |   District cooling grid power | Value |
| **C_districtcool** |   District cooling procurement costs | Timeseries |
| **em_elgrid** |   Emissions electricity grid | Timeseries |
| **em_dhgrid** |   Emissions district heating grid | Timeseries |
| **em_coolgrid** |   Emissions district cooling grid | Timeseries |
| **P_CO2** |   CO2 price | Timeseries |
| **P_elshift** |   Max. electricity load shift | Timeseries |
| **t_elshift** |   Electricity load shift period | Value |
| **P_heatshift** |   Max. heat load shift | Timeseries |
| **t_heatshift** |   Heat load shift period | Value |
| **P_coolshift** |   Max. cooling load shift | Timeseries |
| **t_coolshift** |   Cooling load shift period | Value |


## EEG data structure
The input data for the EEG processes are provided as matrix in a separate excel sheet EEG in [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx).

All consumers are set as column and row indices. If additional consumers were added to the input data, they must also be added to the EEG data.

The values in the EEG data define the grid tariff reduction factor for EEG procurement by consumer $i$ from consumer $j$. It can be differed between local and regional EEG procurement. This is specified with different reduction values in the input data.

<img src="https://latex.codecogs.com/png.latex?\Pi^{EEG,purchase}_{i,j} = \Pi^{EEG,energy} + \Pi^{grid} \cdot F^{gridred}_{i,j} " />

## Load input functions

Different functions are provided that load the required input data into the model.

<details>
<summary>load_scenario</summary>

Loads the scenario setting data into the model. 
The filename of [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx) must be given as a parameter.

```

def load_scenario(filename):
    # Create an ExcelFile object
    df = pd.read_excel(filename, index_col=0, sheet_name="Scenario")
    return df 

```

</details>


<details>
<summary>load_data</summary>

Loads the timeseries input data into the model. 
The filename of [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx) must be given as a parameter. Additionally it must be specified if a test scenario is performed, like defined in the scenario setting.

```

def load_data(filename, test):
    data_struct={}
    # Create an ExcelFile object
    xls = pd.ExcelFile(filename)
    
    # Get the sheet names
    sheet_names = xls.sheet_names
    
    for sheet in sheet_names:
        
        if not ("consumer" in sheet.lower()):
            continue
        
        sheet_struct={}
        if(test):
            data=xls.parse(sheet, nrows=2)
        else:
            data=xls.parse(sheet)
        
        columns=data.columns
        
        sheet_struct[columns[1]] = data[columns[1]].values
        sheet_struct[columns[2]] = data[columns[2]].values[0]
        sheet_struct[columns[3]] = data[columns[3]].values
        sheet_struct[columns[4]] = data[columns[4]].values[0]
        sheet_struct[columns[5]] = data[columns[5]].values[0]
        sheet_struct[columns[6]] = data[columns[6]].values
        sheet_struct[columns[7]] = data[columns[7]].values[0]
        sheet_struct[columns[8]] = data[columns[8]].values
        sheet_struct[columns[9]] = data[columns[9]].values[0]
        sheet_struct[columns[10]] = data[columns[10]].values[0]
        sheet_struct[columns[11]] = data[columns[11]].values[0]
        sheet_struct[columns[12]] = data[columns[12]].values[0]
        sheet_struct[columns[13]] = data[columns[13]].values[0]
        sheet_struct[columns[14]] = data[columns[14]].values
        sheet_struct[columns[15]] = data[columns[15]].values[0]
        sheet_struct[columns[16]] = data[columns[16]].values
        sheet_struct[columns[17]] = data[columns[17]].values[0]
        sheet_struct[columns[18]] = data[columns[18]].values[0]
        sheet_struct[columns[19]] = data[columns[19]].values
        sheet_struct[columns[20]] = data[columns[20]].values[0]
        sheet_struct[columns[21]] = data[columns[21]].values[0]
        sheet_struct[columns[22]] = data[columns[22]].values[0]
        sheet_struct[columns[23]] = data[columns[23]].values[0]
        sheet_struct[columns[24]] = data[columns[24]].values[0]
        sheet_struct[columns[25]] = data[columns[25]].values
        sheet_struct[columns[26]] = data[columns[26]].values[0]
        sheet_struct[columns[27]] = data[columns[27]].values
        sheet_struct[columns[28]] = data[columns[28]].values[0]
        sheet_struct[columns[29]] = data[columns[29]].values[0]
        sheet_struct[columns[30]] = data[columns[30]].values
        sheet_struct[columns[31]] = data[columns[31]].values
        sheet_struct[columns[32]] = data[columns[32]].values
        sheet_struct[columns[33]] = data[columns[33]].values
        sheet_struct[columns[34]] = data[columns[34]].values
        
        data_struct[sheet] = sheet_struct
        
        
    return data_struct


```

</details>


<details>
<summary>load_data_EEG</summary>

Loads the EEG data into the model.
The filename of [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx) must be given as a parameter.

```

def load_data_EEG(filename):
    # Create an ExcelFile object
    df = pd.read_excel(filename, index_col=0, sheet_name="EEG")
    return df  

```

</details>


<details>
<summary>Call of the methods and define model</summary>

The call of the methods to get the input data is done in the model script as follows:

```

# -------------------------------------------------------------------------
    # Input and output files
# -------------------------------------------------------------------------


#Filename of Input Data
filename="Input_Data.xlsx"


#Data output name
filename_out = "results.xlsx"

# -------------------------------------------------------------------------
    # Load scenarios --> iterating over scenarios would be possible with adaption of Code structure
# -------------------------------------------------------------------------


df_scenario = load_data.load_scenario(filename)

#Define if energy community operation is activated
energy_community=df_scenario["Energy_community"].values[0]

#Define if peak power should be aggregated
aggregate_peakpower=df_scenario["Aggregate_Peakpower"].values[0]

#Variable if test run is true
test=df_scenario["Test"].values[0]


#Scenario name
scenario = df_scenario["Scenario"].values[0]

#Resolution of the data
deltaT=df_scenario["Resolution"].values[0]

# -------------------------------------------------------------------------


#Load the Input data
data=load_data.load_data(filename, test)
print("--------- Input data loaded -------------- \n")


#Load the EEG data
if(energy_community):
    data_EEG=load_data.load_data_EEG(filename)
    print("--------- Input data EEG loaded -------------- \n")

#Data keys = consumers
consumers=list(data.keys())


 

```


The model creation is done with the following code:

```

# -------------------------------------------------------------------------
    # create model
# -------------------------------------------------------------------------
m = pyomo.ConcreteModel()
m.name = 'Renvelope_Model'
m.created = datetime.now().strftime('%Y%m%dT%H%M')

```

</details>




## Sets

Different sets must be defined in the model. These include the following sets:

<details>
<summary>Timesteps</summary>

Set with all timesteps to be considered. It is iterated over this set to perform the optimization for all timesteps.

```

#Timesteps based on timeseries - PV was taken, but also possible with other timeseries
m.timesteps = list(range(len(data[consumers[0]]["PV"])))

#Set of timesteps
m.t = pyomo.Set(
        initialize=m.timesteps,
        ordered=True,
        doc='Set of timesteps')


```

</details>


<details>
<summary>Consumers</summary>

All consumers are included as a set, that is automatically defined based on the input data.

```

#Set of consumers

#Input data
data=load_data.load_data(filename, test)

#Data keys = consumers
consumers=list(data.keys())

m.c = pyomo.Set(
        initialize=consumers,
        ordered=True,
        doc='Set of consumers')


```

</details>

<details>
<summary>Consumers to trade</summary>

A second identical set with consumers must be defined for EEG processes. It is required to define the model constraints and objective for trading between consumers.

```

#Set of consumers to trade
m.j = pyomo.Set(
        initialize=consumers,
        ordered=True,
        doc='Set of consumers to trade')


```

</details>



## Variables
Different variables must be defined for the model runs. These are summarized as follows.

<details>
<summary>Electricity grid procurement</summary>

Electricity grid procurement for each consumer at all timesteps.

```

m.q_elgrid = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elgrid",
        doc='Electricitygrid Procurement')


```

</details>

<details>
<summary>Electricity grid feedin</summary>

Electricity grid feedin for each consumer at all timesteps.

```

m.q_feedin = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_feedin",
        doc='Electricitygrid Feedin')


```

</details>

<details>
<summary>Electricity grid peak power</summary>

Electricity grid peak power in the optimization period for each consumer.

```

m.p_max = pyomo.Var(
        m.c,
        within=pyomo.NonNegativeReals,
        name="p_max",
        doc='Electricitygrid maximum power')


```

</details>

<details>
<summary>Variable electricity demand</summary>

Electricity demand for each consumer at each timesteps as variable to enable potential model extensions.

```

m.q_eldemand = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_eldemand",
        doc='Variable electricity demand')



```

</details>

<details>
<summary>Variable PV generation</summary>

PV generation for each consumer at each timesteps as variable to enable potential model extensions.

```

m.q_pv = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_PV",
        doc='Variable PV generation')



```

</details>

<details>
<summary>Battery charging energy</summary>

Battery charging input consumer to battery for each consumer at each timesteps as variable.

```

m.q_batcharge = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_batcharge",
        doc='Battery Charging')



```

</details>

<details>
<summary>Battery discharging energy</summary>

Battery discharging output from battery to consumer for each consumer at each timesteps as variable.

```

m.q_batdischarge = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_batdischarge",
        doc='Battery Discharging')



```

</details>

<details>
<summary>Battery SOC </summary>

Battery variable state of charge for each consumer at each timesteps as variable.

```

m.q_batSOC = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_batSOC",
        doc='Battery SOC')




```

</details>


<details>
<summary>Heat demand </summary>

Variable heat demand for each consumer at each timesteps as variable for potential model extensions.

```

m.q_heatdemand = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_heatdemand",
        doc='Variable heat demand')




```

</details>

<details>
<summary>Heat pump electricity </summary>

Heat pump electricity input for each consumer at each timestep.

```

m.q_HPel = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_HPel",
        doc='Heat pump electricity')




```

</details>

<details>
<summary>Heat pump heat </summary>

Heat pump heat output for each consumer at each timestep.

```

m.q_HPth = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_HPth",
        doc='Heat pump heat')




```

</details>

<details>
<summary> District heat grid procurement </summary>

District heat grid procurement for each consumer at each timestep.

```

m.q_dh = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_dh",
        doc='District heat energy')




```

</details>


<details>
<summary>Thermal storage charging energy</summary>

Thermal storage charging input consumer to thermal storage for each consumer at each timesteps as variable.

```

m.q_thstorecharge = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_thstorecharge",
        doc='Thermal Storage Charging')


```

</details>

<details>
<summary>Thermal storage discharging energy</summary>

Thermal storage discharging output from thermal storage to consumer for each consumer at each timesteps as variable.

```

m.q_thstoredischarge = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_thstoredischarge",
        doc='Thermal Storage Discharging')



```

</details>

<details>
<summary>Thermal storage SOC </summary>

Thermal storage variable state of charge for each consumer at each timesteps.

```

m.q_thstoreSOC = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_thstoreSOC",
        doc='Thermal Storage SOC')



```

</details>

<details>
<summary>Cooling demand </summary>

Cooling demand for each consumer at each timesteps as variable for potential model extensions.

```

m.q_cooldemand = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_cooldemand",
        doc='Variable cooling demand')



```

</details>

<details>
<summary>Electric cooling electricity </summary>

Electric cooling electricity input for each consumer at each timestep.

```

m.q_elCoolel = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elCoolel",
        doc='Electric cooling with heat pump - Electricity input')


```

</details>

<details>
<summary>Electric cooling cooling </summary>

Electric cooling cooling output for each consumer at each timestep.

```

m.q_elCoolcool = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elCoolcool",
        doc='Electric cooling with heat pump - Cooling output')


```

</details>

<details>
<summary>District cooling purchase </summary>

Cooling purchase from district cooling grid for each consumer at each timestep.

```

m.q_dcool = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_dcool",
        doc='District cooling energy')


```

</details>

<details>
<summary>Emissions electricity grid </summary>

Electricity grid emissions for each consumer at each timestep.

```

m.em_elgrid = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="em_elgrid",
        doc='CO2 emissions electricity grid')


```

</details>


<details>
<summary>Emissions district heat grid </summary>

District heat grid emissions for each consumer at each timestep.

```

m.em_dh = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="em_dh",
        doc='CO2 emissions district heating grid')


```

</details>


<details>
<summary>Emissions district cooling grid </summary>

District cooling grid emissions for each consumer at each timestep.

```

m.em_dcool = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="em_dcool",
        doc='CO2 emissions district cooling grid')


```

</details>

<details>
<summary>Total emissions </summary>

Total CO2 emissions for each consumer at each timestep.

```

m.em_total = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="em_total",
        doc='Total CO2 emissions')


```

</details>

<details>
<summary>EEG purchase </summary>

Purchase from all consumers in the EEG by all consumer at each timestep. Only valid if energy community is activated in scenario setting.

```

m.q_c2L = pyomo.Var(
        m.t, m.c, m.j,
        within=pyomo.NonNegativeReals,
        name="q_c2L",
        doc='EEG Purchase')


```

</details>


<details>
<summary>EEG sale </summary>

Sale by all consumers in the EEG to all consumer at each timestep. Only valid if energy community is activated in scenario setting.

```

m.q_L2c = pyomo.Var(
        m.t, m.c, m.j,
        within=pyomo.NonNegativeReals,
        name="q_L2c",
        doc='EEG Sale')


```

</details>

<details>
<summary>Aggregated peak power </summary>

Peak power that emerges from aggregated electricity grid purchase as summation for all consumers' electricity grid procurement. Only valid if aggregate peakpower is activated in scenario setting.

```

m.p_max_agg = pyomo.Var(
        within=pyomo.NonNegativeReals,
        name="p_max_agg",
        doc='Electricitygrid maximum power aggregated')


```

</details>


<details>
<summary>Electricity load increase </summary>

Load increase for electricity load shift at each timestep.

```

m.q_elshiftup = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elshiftup",
        doc='Electricity load upshift')


```

</details>

<details>
<summary>Electricity load decrease </summary>

Load decrease for electricity load shift at each timestep.

```

m.q_elshiftdown = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elshiftdown",
        doc='Electricity load downshift')


```

</details>


<details>
<summary>Heat load increase </summary>

Load increase for heat load shift at each timestep.

```

m.q_heatshiftup = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_heatshiftup",
        doc='Heat load upshift')

```

</details>

<details>
<summary>Heat load decrease </summary>

Load decrease for heat load shift at each timestep.

```

m.q_heatshiftdown = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_heatshiftdown",
        doc='Heat load downshift')


```

</details>


<details>
<summary>Cooling load increase </summary>

Load increase for cooling load shift at each timestep.

```

m.q_coolshiftup = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_coolshiftup",
        doc='Cooling load upshift')


```

</details>

<details>
<summary>Cooling load decrease </summary>

Load decrease for cooling load shift at each timestep.

```

m.q_coolshiftdown = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_coolshiftdown",
        doc='Cooling load downshift')


```

</details>


## Constraints

The optimization model includes several constraints that are summarized as follows.


<details>
<summary>Variable electricity demand coverage </summary>

The variable electricity demand must cover the pre-defined electricity demand parameter. This constraint is established for potential model extensions.

<img src="https://latex.codecogs.com/png.latex? q^{eldemand}_{i,t} =  Q^{eldemand}_{i,t} " />

```

def elecdemand_rule(m, t, c):
    return (m.q_eldemand[t, c] 
            == data[c]["D_el"][t])
m.elecdemand = pyomo.Constraint(
    m.t, m.c, 
    rule=elecdemand_rule,
    doc='Electricity demand rule')


```

</details>


<details>
<summary>PV generation </summary>

The variable PV generation is implemented as product of the peak capacity and the generation timeseries. This constraint is established for potential model extensions.

<img src="https://latex.codecogs.com/png.latex? q^{pv}_{i,t} =  F^{PV}_{i,t} \cdot P^{PV}_{i}" />

```

def pv_rule(m, t, c):
    return (m.q_pv[t, c] 
            == data[c]["PV"][t]*data[c]["P_pv"])
m.pv = pyomo.Constraint(
    m.t, m.c, 
    rule=pv_rule,
    doc='PV generation rule')


```

</details>


<details>
<summary>Grid procurement power limit </summary>

Grid procurement each timestep is limited by the maximum grid connection power and the resolution of the model.

<img src="https://latex.codecogs.com/png.latex? q^{elgrid}_{i,t} \le  P^{elgrid}_{i} \cdot \Delta t" />

```

def maxgrid_rule(m, t, c):
    return (m.q_elgrid[t, c] 
            <= data[c]["P_elgrid"]*deltaT)
m.maxgrid = pyomo.Constraint(
    m.t, m.c, 
    rule=maxgrid_rule,
    doc='Maximum grid procurement power')


```

</details>


<details>
<summary>Grid feedin power limit </summary>

Grid feedin each timestep is limited by the maximum grid connection power.

<img src="https://latex.codecogs.com/png.latex? q^{feedin}_{i,t} \le  P^{elgrid}_{i} \cdot \Delta t" />

```

def maxfeedin_rule(m, t, c):
    return (m.q_feedin[t, c] 
            <= data[c]["P_elgrid"]*deltaT)
m.maxfeedin = pyomo.Constraint(
    m.t, m.c, 
    rule=maxfeedin_rule,
    doc='Maximum grid feedin')


```

</details>


<details>
<summary>Battery charging limit </summary>

Battery charging is limited by the defined charging power and the resolution of the model.

<img src="https://latex.codecogs.com/png.latex? q^{batcharge}_{i,t} \le  P^{bat}_{i} \cdot \Delta t" />

```

def maxcharge_rule(m, t, c):
    return (m.q_batcharge[t, c] 
            <= data[c]["P_bat"]*deltaT)
m.maxcharge = pyomo.Constraint(
    m.t, m.c, 
    rule=maxcharge_rule,
    doc='Maximum battery charge power')



```

</details>


<details>
<summary>Battery discharging limit </summary>

Battery discharging is limited by the defined charging power and the resolution of the model.

<img src="https://latex.codecogs.com/png.latex? q^{batdischarge}_{i,t} \le P^{bat}_{i} \cdot \Delta t" />

```

def maxdischarge_rule(m, t, c):
    return (m.q_batdischarge[t, c] 
            <= data[c]["P_bat"]*deltaT)
m.maxdischarge = pyomo.Constraint(
    m.t, m.c, 
    rule=maxdischarge_rule,
    doc='Maximum battery discharge power')



```

</details>


<details>
<summary>Battery balance rule </summary>

The battery SOC is lowered by the standby efficiency each timestep. Charging of the battery is multiplied by the battery efficiency and added to the SOC. Discharging is divided by the efficiency and subtracted from the SOC, as more SOC decrease emerges due to efficiency consideration. The balance rule is not valid for the first timestep and is theerefore deactivated for this timestep. 

<img src="https://latex.codecogs.com/png.latex? soc^{bat}_{i,t} = soc^{bat}_{i,t-1} \cdot \eta^{batSB} + q^{batcharge}_{i,t} \cdot \eta^{bat} - q^{batdischarge}_{i,t}/\eta^{bat} \quad \forall t \ge 1" />

```

def batterybalance_rule(m, t, c):
    if t == m.timesteps[0]: 
            return pyomo.Constraint.Skip
    else:
        return (m.q_batSOC[t,c] == m.q_batSOC[t-1, c] * data[c]["Eta_batSB"] + m.q_batcharge[t, c] * data[c]["Eta_bat"] - m.q_batdischarge[t, c] / data[c]["Eta_bat"])
m.batterybalance = pyomo.Constraint(
    m.t, m.c, 
    rule=batterybalance_rule,
    doc='Battery balance')



```

</details>


<details>
<summary>Battery initial and end rule </summary>

As additionally considered constraints, the battery SOC is assumed to be at the same state at the beginning and end of the model run.

<img src="https://latex.codecogs.com/png.latex? soc^{bat}_{i,0} = soc^{bat}_{i,T} " />

```

def sto_initial_and_end_rule(m, c): 
    return (m.q_batSOC[0,c]
            == m.q_batSOC[m.timesteps[-1], c])
m.sto_initial_and_end = pyomo.Constraint(
    m.c,
    rule=sto_initial_and_end_rule,
    doc='storage content initial == end')



```

</details>


<details>
<summary>Battery initial value </summary>

An initial SOC for the battery must be defined which is implemented as additional constraint. Default, an initial storage value of zero is assumed.

<img src="https://latex.codecogs.com/png.latex? soc^{bat}_{i,0} = SOC^{bat,start}_{i} " />

```

def sto_initial_rule(m, c): 
        return (m.q_batSOC[m.timesteps[0], c] == 0)
m.sto_initial = pyomo.Constraint(
   m.c,
   rule=sto_initial_rule,
   doc='storage content initial == 0')



```

</details>


<details>
<summary>Battery initial discharge rule </summary>

The discharging of the storage in the initial step is limited by the SOC at the beginning. This is implemented to avoid ignoring the battery balance in the inital timestep.

<img src="https://latex.codecogs.com/png.latex? q^{batdischarge}_{i,0} \le soc^{bat}_{i,0} " />

```

def discharge_initial_rule(m, c): 
        return (m.q_batdischarge[m.timesteps[0], c] <= m.q_batSOC[m.timesteps[0], c])
m.discharge_initial = pyomo.Constraint(
   m.c,
   rule=discharge_initial_rule,
   doc='Discharge initial <= Storage Initial ')



```

</details>


<details>
<summary>Maximal SOC rule battery </summary>

The battery SOC is limited by a pre-defined maximum SOC value.

<img src="https://latex.codecogs.com/png.latex? soc^{bat}_{i,0} \le SOC^{bat,max}_{i}" />

```

def sto_max1_rule(m, t, c): 
    return (m.q_batSOC[t, c] \
            <= (data[c]["SOC_bat"]))
m.sto_max1 = pyomo.Constraint(
    m.t, m.c,
    rule=sto_max1_rule,
    doc='storage soc <= max storage soc')



```

</details>




<details>
<summary>Thermal storage charging limit </summary>

Thermal storage charging is limited by the defined charging power and the resolution of the model.

<img src="https://latex.codecogs.com/png.latex? q^{thstorecharge}_{i,t} \le  P^{thstore}_{i} \cdot \Delta t" />

```

def maxchargethstore_rule(m, t, c):
    return (m.q_thstorecharge[t, c] 
            <= data[c]["P_thstore"]*deltaT)
m.maxchargethstore = pyomo.Constraint(
    m.t, m.c, 
    rule=maxchargethstore_rule,
    doc='Maximum Thermal Storage charge power')



```

</details>


<details>
<summary>Thermal storage discharging limit </summary>

Thermal storage discharging is limited by the defined charging power and the resolution of the model.

<img src="https://latex.codecogs.com/png.latex? q^{thstoredischarge}_{i,t} \le  P^{thstore}_{i} \cdot \Delta t" />

```

def maxdischargethstore_rule(m, t, c):
    return (m.q_thstoredischarge[t, c] 
            <= data[c]["P_thstore"]*deltaT)
m.maxdischargethstore = pyomo.Constraint(
    m.t, m.c, 
    rule=maxdischargethstore_rule,
    doc='Maximum Thermal Storage discharge power')



```

</details>


<details>
<summary>Thermal storage balance rule </summary>

The thermal storage SOC is lowered by the standby efficiency each timestep. Charging of the thermal storage is multiplied by the thermal storage efficiency and added to the SOC. Discharging is divided by the efficiency and subtracted from the SOC, as more SOC decrease emerges due to efficiency consideration. The balance rule is not valid for the first timestep and is theerefore deactivated for this timestep. 

<img src="https://latex.codecogs.com/png.latex? soc^{thstore}_{i,t} = soc^{thstore}_{i,t-1} \cdot \eta^{thstoreSB} + q^{thstorecharge}_{i,t} \cdot \eta^{thstore} - q^{thstoredischarge}_{i,t}/\eta^{thstore} \quad \forall t \ge 1" />

```

def thstorebalance_rule(m, t, c):
    if t == m.timesteps[0]: 
            return pyomo.Constraint.Skip
    else:
        return (m.q_thstoreSOC[t,c] == m.q_thstoreSOC[t-1, c] * data[c]["Eta_thstoreSB"] + m.q_thstorecharge[t, c] * data[c]["Eta_thstore"] - m.q_thstoredischarge[t, c] / data[c]["Eta_thstore"])
m.thstorebalance = pyomo.Constraint(
    m.t, m.c, 
    rule=thstorebalance_rule,
    doc='Thermal storage balance')



```

</details>


<details>
<summary>Thermal storage initial and end rule </summary>

As additionally considered constraints, the thermal storage SOC is assumed to be at the same state at the beginning and end of the model run.

<img src="https://latex.codecogs.com/png.latex? soc^{thstore}_{i,0} = soc^{thstore}_{i,T} " />

```

def thstore_initial_and_end_rule(m, c): 
    return (m.q_thstoreSOC[0,c]
            == m.q_thstoreSOC[m.timesteps[-1], c])
m.thstore_initial_and_end = pyomo.Constraint(
    m.c,
    rule=thstore_initial_and_end_rule,
    doc='Thermal storage content initial == end')



```

</details>


<details>
<summary>Thermal storage initial value </summary>

An initial SOC for the thermal storage must be defined which is implemented as additional constraint. Default, an initial storage value of zero is assumed.

<img src="https://latex.codecogs.com/png.latex? soc^{thstore}_{i,0} = SOC^{thstore,start}_{i} " />

```

def thstore_initial_rule(m, c): 
        return (m.q_thstoreSOC[m.timesteps[0], c] == 0)
m.thstore_initial = pyomo.Constraint(
   m.c,
   rule=thstore_initial_rule,
   doc='Thermal storage content initial == 0')



```

</details>


<details>
<summary>Thermal storage initial discharge rule </summary>

The discharging of the storage in the initial step is limited by the SOC at the beginning. This is implemented to avoid ignoring the thermal storage balance in the inital timestep.

<img src="https://latex.codecogs.com/png.latex? q^{thstoredischarge}_{i,0} \le soc^{thstore}_{i,0} " />

```

def thstore_discharge_initial_rule(m, c): 
        return (m.q_thstoredischarge[m.timesteps[0], c] <= m.q_thstoreSOC[m.timesteps[0], c])
m.thstore_discharge_initial = pyomo.Constraint(
   m.c,
   rule=thstore_discharge_initial_rule,
   doc='Thermal storage Discharge initial <= Thermal Storage Initial ')



```

</details>


<details>
<summary>Maximal SOC rule thermal storage</summary>

The thermal storage SOC is limited by a pre-defined maximum SOC value.

<img src="https://latex.codecogs.com/png.latex? soc^{thstore}_{i,0} \le SOC^{thstore,max}_{i}" />

```

def thstore_max1_rule(m, t, c): 
    return (m.q_thstoreSOC[t, c] \
            <= (data[c]["SOC_thstore"]))
m.thstore_max1 = pyomo.Constraint(
    m.t, m.c,
    rule=thstore_max1_rule,
    doc='Thermal storage soc <= max thermal storage soc')



```

</details>


<details>
<summary>Variable heat demand coverage </summary>

The variable heat demand must cover the pre-defined heat demand parameter. This constraint is established for potential model extensions.

<img src="https://latex.codecogs.com/png.latex? q^{heatdemand}_{i,t} =  Q^{heatdemand}_{i,t} " />

```

def heatdemand_rule(m, t, c):
    return (m.q_heatdemand[t, c] 
            == data[c]["D_th"][t])
m.heatdemand = pyomo.Constraint(
    m.t, m.c, 
    rule=heatdemand_rule,
    doc='Heat demand rule')


```

</details>


<details>
<summary>Heat pump relation rule </summary>

The heat pump electricity and heat are related with the time-dependent COP input parameter. This relation is implemented as model constraint.

<img src="https://latex.codecogs.com/png.latex? q^{hp,th}_{i,t} =  q^{hp,el}_{i,t} \cdot COP^{hp}_{i,t}" />

```

def hp_rule(m, t, c): 
    return (m.q_HPel[t, c]*data[c]["COP"][t] \
            == m.q_HPth[t, c])
m.hp = pyomo.Constraint(
    m.t, m.c,
    rule=hp_rule,
    doc='Heat pump input/output relation')


```

</details>


<details>
<summary>Heat pump thermal power limit</summary>

The thermal power limit of the heat pump.

<img src="https://latex.codecogs.com/png.latex? q^{hp,th}_{i,t} \le  P^{hp}_{i}" />

```

def hpmax_rule(m, t, c):
    return (m.q_HPth[t, c] 
            <= data[c]["P_hp"]*deltaT)
m.hpmax = pyomo.Constraint(
    m.t, m.c, 
    rule=hpmax_rule,
    doc='Maximum heat pump heat')


```

</details>


<details>
<summary>Heat pump electric power limit</summary>

The electric power limit of the heat pump. Not really relevant with given COP values as the thermal constraint is the binding constraint. Only valid in potential model extensions

<img src="https://latex.codecogs.com/png.latex? q^{hp,el}_{i,t} \le  P^{hp}_{i}" />

```

def hpmaxel_rule(m, t, c):
    return (m.q_HPel[t, c] 
            <= data[c]["P_hp"]*deltaT)
m.hpmaxel = pyomo.Constraint(
    m.t, m.c, 
    rule=hpmaxel_rule,
    doc='Maximum heat pump electricity')



```

</details>

<details>
<summary>District heat grid procurement power limit </summary>

District heat grid procurement each timestep is limited by the maximum grid connection power and the resolution of the model.

<img src="https://latex.codecogs.com/png.latex? q^{dhgrid}_{i,t} \le  P^{dhrid}_{i} \cdot \Delta t" />

```

def maxdh_rule(m, t, c):
    return (m.q_dh[t, c] 
            <= data[c]["P_districtheat"]*deltaT)
m.maxdh = pyomo.Constraint(
    m.t, m.c, 
    rule=maxdh_rule,
    doc='Maximum district heat procurement power')


```

</details>



<details>
<summary>Variable cooling demand coverage </summary>

The variable cooling demand must cover the pre-defined cooling demand parameter. This constraint is established for potential model extensions.

<img src="https://latex.codecogs.com/png.latex? q^{cooldemand}_{i,t} =  Q^{cooldemand}_{i,t} " />

```

def cooldemand_rule(m, t, c):
    return (m.q_cooldemand[t, c] 
            == data[c]["D_cool"][t])
m.cooldemand = pyomo.Constraint(
    m.t, m.c, 
    rule=cooldemand_rule,
    doc='Cooling demand rule')



```

</details>


<details>
<summary>Electric cooling cooling power limit</summary>

The cooling power limit of the electric cooler.

<img src="https://latex.codecogs.com/png.latex? q^{elcool,cool}_{i,t} \le  P^{elcool}_{i}" />

```

def elcoolmax_rule(m, t, c):
    return (m.q_elCoolcool[t, c] 
            <= data[c]["P_thcool"]*deltaT)
m.elcoolmax = pyomo.Constraint(
    m.t, m.c, 
    rule=elcoolmax_rule,
    doc='Maximum electric cooling cooling')


```

</details>


<details>
<summary>Electric cooling electric power limit</summary>

The electric power limit of the electric cooling. Not really relevant with given COP values as the thermal constraint is the binding constraint. Only valid in potential model extensions

<img src="https://latex.codecogs.com/png.latex? q^{elcool,el}_{i,t} \le  P^{elcool}_{i}" />

```

def elcoolmaxel_rule(m, t, c):
    return (m.q_elCoolel[t, c] 
            <= data[c]["P_thcool"]*deltaT)
m.elcoolmaxel = pyomo.Constraint(
    m.t, m.c, 
    rule=elcoolmaxel_rule,
    doc='Maximum electric cooling electricity')



```

</details>


<details>
<summary>Electric cooling relation rule </summary>

The electric cooling electricity and cooling are related with the time-dependent COP cooling input parameter. This relation is implemented as model constraint.

<img src="https://latex.codecogs.com/png.latex? q^{elcool,cool}_{i,t} =  q^{elcool,el}_{i,t} \cdot COP^{elcool}_{i,t}" />

```

def elcool_rule(m, t, c): 
    return (m.q_elCoolel[t, c]*data[c]["COP_cool"][t] \
            == m.q_elCoolcool[t, c])
m.elcool = pyomo.Constraint(
    m.t, m.c,
    rule=elcool_rule,
    doc='Electric cooling input/output relation')



```

</details>

<details>
<summary>District cooling grid procurement power limit </summary>

District cooling grid procurement each timestep is limited by the maximum grid connection power and the resolution of the model.

<img src="https://latex.codecogs.com/png.latex? q^{dcoolgrid}_{i,t} \le  P^{dcoolrid}_{i} \cdot \Delta t" />

```

def maxdcool_rule(m, t, c):
    return (m.q_dcool[t, c] 
            <= data[c]["P_districtcool"]*deltaT)
m.maxdcool = pyomo.Constraint(
    m.t, m.c, 
    rule=maxdcool_rule,
    doc='Maximum district cooling procurement power')


```

</details>


<details>
<summary>Emissions electricity grid </summary>

Emission relation for the electricity grid, where the electricity grid procurement for each consumer is multiplied by pre-defined, time-dependent emission factors.

<img src="https://latex.codecogs.com/png.latex? em^{elgrid}_{i,t} =  F^{CO2,elgrid}_{i,t} \cdot q^{elgrid}_{i,t}" />

```

def emelgrid_rule(m, t, c):
    return (m.q_elgrid[t, c] *  data[c]["em_elgrid"][t]
            == m.em_elgrid[t, c])
m.emelgrid = pyomo.Constraint(
    m.t, m.c, 
    rule=emelgrid_rule,
    doc='Emissions electricity grid relation')


```

</details>


<details>
<summary>Emissions district heating grid </summary>

Emission relation for the district heating grid, where the heat grid procurement for each consumer is multiplied by pre-defined, time-dependent emission factors.

<img src="https://latex.codecogs.com/png.latex? em^{dhgrid}_{i,t} =  F^{CO2,dhgrid}_{i,t} \cdot q^{dhgrid}_{i,t}" />

```

def emdhgrid_rule(m, t, c):
    return (m.q_dh[t, c] *  data[c]["em_dhgrid"][t]
            == m.em_dh[t, c])
m.emdhgrid = pyomo.Constraint(
    m.t, m.c, 
    rule=emdhgrid_rule,
    doc='Emissions district heat grid relation')


```

</details>


<details>
<summary>Emissions district cooling grid </summary>

Emission relation for the district cooling grid, where the cooling grid procurement for each consumer is multiplied by pre-defined, time-dependent emission factors.

<img src="https://latex.codecogs.com/png.latex? em^{dcoolgrid}_{i,t} =  F^{CO2,dcoolgrid}_{i,t} \cdot q^{dcoolgrid}_{i,t}" />

```

def emdcoolgrid_rule(m, t, c):
    return (m.q_dcool[t, c] *  data[c]["em_coolgrid"][t]
            == m.em_dcool[t, c])
m.emdcoolgrid = pyomo.Constraint(
    m.t, m.c, 
    rule=emdcoolgrid_rule,
    doc='Emissions district cooling grid relation')


```

</details>


<details>
<summary>Electricity balance rule</summary>

The electricity balance rule is implemented for each consumer. Inputs come from the electricity grid, PV generation, battery discharge and outputs from battery charging, heat pump electricity, fed in electricity, electric cooling operation and electricity demand coverage. Additionally, flexible load shifts are considered

Additional parameters must be appended if energy communities are considered. These include community purchase and sale for all consumers.

<img src="https://latex.codecogs.com/png.latex? b^{el} = q^{elgrid}_{i,t} + q^{pv}_{i,t} + q^{batdischarge}_{i,t} -  q^{batcharge}_{i,t} - q^{hp,el}_{i,t} - q^{feedin}_{i,t} - q^{elcool,el}_{i,t} - q^{eldemand}_{i,t} - q^{elshiftup}_{i,t} + q^{elshiftdown}_{i,t} = 0" />

An adaption for EEG consideration leads to the following balance rule.

<img src="https://latex.codecogs.com/png.latex? b^{el,com} = b^{el} + \sum_{j \in Consumers, i \neq j}{q^{c2L}_{i,j,t}-q^{L2c}_{i,j,t}} = 0 " />


```

def elecbalance_rule(m, t, c):
    expr=0
    if(energy_community):
        
        expr= (m.q_elgrid[t, c] + m.q_pv[t, c] + m.q_batdischarge[t, c] - m.q_batcharge[t, c] - m.q_HPel[t, c] -  m.q_feedin[t, c] - m.q_elCoolel[t, c])
        expr -= m.q_eldemand[t, c]
        expr -= m.q_elshiftup[t,c]+m.q_elshiftdown[t,c]
        expr+=sum(m.q_c2L[t, c, j] for j in m.j if c!=j)
        expr-=sum(m.q_L2c[t, c, j] for j in m.j if c!=j)
    
        
        return expr==0
            
    else:
        expr= (m.q_elgrid[t, c] + m.q_pv[t, c] + m.q_batdischarge[t, c] - m.q_batcharge[t, c] - m.q_HPel[t, c] -  m.q_feedin[t, c] - m.q_elCoolel[t, c])
        expr -= m.q_eldemand[t, c]
        expr -= m.q_eldemand[t, c]
        expr -= m.q_elshiftup[t,c]+m.q_elshiftdown[t,c]
        return expr==0

m.elecbalance = pyomo.Constraint(
    m.t, m.c,
    rule=elecbalance_rule,
    doc='Electricity balance rule')



```

</details>


<details>
<summary>Heat balance rule</summary>

The heat balance rule is implemented for each consumer. Inputs come from the heat pump, thermal storage discharging and district heating grid while outputs are demand coverage and thermal storage charging.
Heat demand shifts must also be considered.


<img src="https://latex.codecogs.com/png.latex? q^{hp,th}_{i,t} + q^{dhgrid}_{i,t} + q^{thstoredischarge}_{i,t} - q^{thstorecharge}_{i,t} - q^{heatdemand}_{i,t} - q^{heatshiftup}_{i,t} + q^{heatshiftdown}_{i,t}= 0" />



```

def heatbalance_rule(m, t, c): 
    return (m.q_HPth[t, c] + m.q_dh[t, c] + m.q_thstoredischarge[t, c] - m.q_thstorecharge[t, c]  - m.q_heatshiftup[t,c] + m.q_heatshiftdown[t,c]\
            == m.q_heatdemand[t, c])
m.heatbalance = pyomo.Constraint(
    m.t, m.c,
    rule=heatbalance_rule,
    doc='Heat balance rule')



```

</details>


<details>
<summary>Cooling balance rule</summary>

The cooling balance rule is implemented for each consumer. Inputs come from the electric cooling and district cooing grid while outputs are demand coverage.
Cooling demand shifts must also be considered.

<img src="https://latex.codecogs.com/png.latex? q^{elcool,cool}_{i,t} + q^{dcoolgrid}_{i,t} - q^{cooldemand}_{i,t} - q^{coolshiftup}_{i,t} + q^{coolshiftdown}_{i,t} = 0" />



```

def coolbalance_rule(m, t, c): 
    return (m.q_elCoolcool[t, c] + m.q_dcool[t, c]- m.q_coolshiftup[t,c] + m.q_coolshiftdown[t,c]\
            == m.q_cooldemand[t, c])
m.coolbalance = pyomo.Constraint(
    m.t, m.c,
    rule=coolbalance_rule,
    doc='Cooling balance rule')


```

</details>


<details>
<summary>Emission balance rule</summary>

All emerging emissions are summarized in a total emissions variable for each consumer. Emissions emerge from electricity grid, district heating grid and district cooling grid purchase.


<img src="https://latex.codecogs.com/png.latex? em^{elgrid}_{i,t} + em^{dhgrid}_{i,t} + em^{dcoolgrid}_{i,t} = em^{tot}_{i,t}" />



```

def emissionbalance_rule(m, t, c): 
    return ( m.em_elgrid[t, c] +  m.em_dh[t, c] +  m.em_dcool[t, c]\
            == m.em_total[t, c])
m.emissionbalance = pyomo.Constraint(
    m.t, m.c,
    rule=emissionbalance_rule,
    doc='Total emission calculation')



```

</details>


<details>
<summary>Electricity grid peak power determination</summary>

The electricity grid procurement peak power over the year for each consumer must be determined. This is done by implementing a variable peak power and a constraint. The peak power variable must be greater than the grid procurement at each time step divided by the resolution. This guarantees that the maximum grid procurement is always considered.


<img src="https://latex.codecogs.com/png.latex? p^{max}_{i} \ge q^{elgrid}_{i,t} / \Delta t" />



```

def peakpower_rule(m, t, c): 
    return (m.p_max[c] \
            >= m.q_elgrid[t, c] / deltaT)
m.peakpower = pyomo.Constraint(
    m.t, m.c, 
    rule=peakpower_rule,
    doc='Peak power rule')



```

</details>


<details>
<summary>Community purchase limitation</summary>

The electricity purchase from the community is limited by the maximum grid power.


<img src="https://latex.codecogs.com/png.latex? q^{c2L}_{t, i, j} \le P^{elgrid}_{i}" />



```

def compeakpowerin_rule(m, t, c, j):
    return (m.q_c2L[t, c, j] 
            <= data[c]["P_elgrid"]*deltaT)
m.compeakpowerin = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=compeakpowerin_rule,
    doc='Maximum community purchase')



```

</details>


<details>
<summary>Community sale limitation</summary>

The electricity to from the community is limited by the maximum grid power.


<img src="https://latex.codecogs.com/png.latex? q^{L2c}_{i, j, t} \le P^{elgrid}_{i}" />



```

def compeakpowerout_rule(m, t, c, j):
    return (m.q_L2c[t, c, j] 
            <= data[c]["P_elgrid"]*deltaT)
m.compeakpowerout = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=compeakpowerout_rule,
    doc='Maximum community sale')



```

</details>


<details>
<summary>Community balance rule</summary>

The community sale from consumer $i$ to consumer $j$ must be equal to the community purchase by consumer $j$ from consumer $i$. This balance rule is implemented as a constraint.


<img src="https://latex.codecogs.com/png.latex? q^{L2c}_{i, j, t} = q^{c2L}_{i, j, t}" />



```

def combalance_rule(m, t, c, j):
    if(energy_community):
        
        return(m.q_c2L[t, c, j] == m.q_L2c[t, j, c])
    

            
    else:
         return pyomo.Constraint.Skip

m.combalance = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=combalance_rule,
    doc='Community balance rule')



```

</details>


<details>
<summary>Community own purchase</summary>

Consumers cannot purchase and sale community energy within the own system boundaries (own sale-purchase not possible). This is implemented as two constraint that avoids the balance rule for same indices.


<img src="https://latex.codecogs.com/png.latex? q^{c2L}_{i, j, t} = 0 \forall \quad i \neq j" />



```

def comownpurchase_rule(m, t, c, j):
    if(energy_community):
        if(j==c):
            return(m.q_c2L[t, c, j] == 0)
        else:
            return pyomo.Constraint.Skip
    else:
        return pyomo.Constraint.Skip

m.comownpurchase = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=comownpurchase_rule,
    doc='Community own purchase prevention')



```


<img src="https://latex.codecogs.com/png.latex? q^{L2c}_{t, i, j} = 0 \forall \quad i \neq j" />



```

def comownsale_rule(m, t, c, j):
    if(energy_community):
        if(j==c):
            return(m.q_L2c[t, c, j] == 0)
        else:
            return pyomo.Constraint.Skip
    else:
        return pyomo.Constraint.Skip

m.comownsale = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=comownsale_rule,
    doc='Community own sale prevention')



```


</details>

<details>
<summary>Aggregated peak power</summary>

The aggregated peak power is determined in a constraint that sums the electricity grid purchases for all consumers at all timesteps. The variable aggregated peak power must be greater than the aggregated electricity grid purchase at all time steps, which ensures that the maximum aggregated power is determined.


<img src="https://latex.codecogs.com/png.latex? p^{max,agg}_{i} \ge \frac{1}{\Delta t} \cdot \sum_{i \in Consumers}{q^{elgrid}_{i,t}} " />



```

def peakpower_rule_aggregated(m, t): 
    if(energy_community and aggregate_peakpower):
        return (m.p_max_agg \
                >= sum(m.q_elgrid[t, c] / deltaT for c in m.c))
    else:
        return pyomo.Constraint.Skip
    
m.peakpower_agg = pyomo.Constraint(
    m.t,
    rule=peakpower_rule_aggregated,
    doc='Peak power rule aggregated')



```

</details>


<details>
<summary>Flexible electricity load increase limit</summary>

The flexible electricity load increase is limited by a pre-defined, time-dependent load shifting power.


<img src="https://latex.codecogs.com/png.latex? q^{elshiftup}_{i,t} \le P^{elshift}_{i,t}" />



```

def maxelup_rule(m, t, c):
    return (m.q_elshiftup[t, c] 
            <= data[c]["P_elshift"][t]*deltaT)
m.maxelup = pyomo.Constraint(
    m.t, m.c, 
    rule=maxelup_rule,
    doc='Maximum electricity upshift')



```

</details>


<details>
<summary>Flexible electricity load decrease limit</summary>

The flexible electricity load decrease is limited by a pre-defined, time-dependent load shifting power.


<img src="https://latex.codecogs.com/png.latex? q^{elshiftdown}_{i,t} \le P^{elshift}_{i,t}" />



```

def maxeldown_rule(m, t, c):
    return (m.q_elshiftdown[t, c] 
            <= data[c]["P_elshift"][t]*deltaT)
m.maxeldown = pyomo.Constraint(
    m.t, m.c, 
    rule=maxeldown_rule,
    doc='Maximum electricity downshift')



```

</details>

<details>
<summary>Electricity load shift balance rule</summary>

Electricity load shifts must be compensated in defined time blocks, based on the pre-defined shifting period. Within a shifting period, the balance rule must be valid.
This guarantees that no general load increases or decreases emerge.


<img src="https://latex.codecogs.com/png.latex? \sum_{t}^{T^{shift}}{q^{elshiftup}_{i,t}-q^{elshiftdown}_{i,t}}=0" />



```

def elshiftbalance_rule(m, t, c):
    expr=0
    if(data[c]["t_elshift"]==0):
        return (m.q_elshiftup[t*data[c]["t_elshift"],c]-m.q_elshiftdown[t*data[c]["t_elshift"],c]==0)
    if((t*data[c]["t_elshift"]>len(m.timesteps)-1)):
        return pyomo.Constraint.Skip
    for i in range(0, int(data[c]["t_elshift"])):
        if((t*data[c]["t_elshift"]+i)<len(m.timesteps)):
            expr+=m.q_elshiftup[t*data[c]["t_elshift"]+i,c]-m.q_elshiftdown[t*data[c]["t_elshift"]+i,c]
    return (expr==0)
m.elshiftbalance = pyomo.Constraint(
    m.t, m.c, 
    rule=elshiftbalance_rule,
    doc='Electricity shift balance rule')




```

</details>


<details>
<summary>Flexible heat load increase limit</summary>

The flexible heat load increase is limited by a pre-defined, time-dependent load shifting power.


<img src="https://latex.codecogs.com/png.latex? q^{heatshiftup}_{i,t} \le P^{heatshift}_{i,t}" />



```

def maxheatup_rule(m, t, c):
    return (m.q_heatshiftup[t, c] 
            <= data[c]["P_heatshift"][t]*deltaT)
m.maxheatup = pyomo.Constraint(
    m.t, m.c, 
    rule=maxheatup_rule,
    doc='Maximum heat upshift')



```

</details>


<details>
<summary>Flexible heat load decrease limit</summary>

The flexible heat load decrease is limited by a pre-defined, time-dependent load shifting power.


<img src="https://latex.codecogs.com/png.latex? q^{heatshiftdown}_{i,t} \le P^{heatshift}_{i,t}" />



```

def maxheatdown_rule(m, t, c):
    return (m.q_heatshiftdown[t, c] 
            <= data[c]["P_heatshift"][t]*deltaT)
m.maxheatdown = pyomo.Constraint(
    m.t, m.c, 
    rule=maxheatdown_rule,
    doc='Maximum heat downshift')



```

</details>

<details>
<summary>Heat load shift balance rule</summary>

Heat load shifts must be compensated in defined time blocks, based on the pre-defined shifting period. Within a shifting period, the balance rule must be valid.
This guarantees that no general load increases or decreases emerge.


<img src="https://latex.codecogs.com/png.latex? \sum_{t}^{T^{shift}}{q^{heatshiftup}_{i,t}-q^{heatshiftdown}_{i,t}}=0" />



```

def heatshiftbalance_rule(m, t, c):
    expr=0
    if(data[c]["t_heatshift"]==0):
        return (m.q_heatshiftup[t*data[c]["t_heatshift"],c]-m.q_heatshiftdown[t*data[c]["t_heatshift"],c]==0)
    if((t*data[c]["t_heatshift"]>len(m.timesteps)-1)):
        return pyomo.Constraint.Skip
    for i in range(0, int(data[c]["t_heatshift"])):
        if((t*data[c]["t_heatshift"]+i)<len(m.timesteps)):
            expr+=m.q_heatshiftup[t*data[c]["t_heatshift"]+i,c]-m.q_heatshiftdown[t*data[c]["t_heatshift"]+i,c]
    return (expr==0)
m.heatshiftbalance = pyomo.Constraint(
    m.t, m.c, 
    rule=heatshiftbalance_rule,
    doc='Heat shift balance rule')


```

</details>


<details>
<summary>Flexible cooling load increase limit</summary>

The flexible cooling load increase is limited by a pre-defined, time-dependent load shifting power.


<img src="https://latex.codecogs.com/png.latex? q^{coolshiftup}_{i,t} \le P^{coolshift}_{i,t}" />



```

def maxcoolup_rule(m, t, c):
    return (m.q_coolshiftup[t, c] 
            <= data[c]["P_coolshift"][t]*deltaT)
m.maxcoolup = pyomo.Constraint(
    m.t, m.c, 
    rule=maxcoolup_rule,
    doc='Maximum cooling upshift')



```

</details>


<details>
<summary>Flexible cooling load decrease limit</summary>

The flexible cooling load decrease is limited by a pre-defined, time-dependent load shifting power.


<img src="https://latex.codecogs.com/png.latex? q^{coolshiftdown}_{i,t} \le P^{coolshift}_{i,t}" />



```

def maxcooldown_rule(m, t, c):
    return (m.q_coolshiftdown[t, c] 
            <= data[c]["P_coolshift"][t]*deltaT)
m.maxcooldown = pyomo.Constraint(
    m.t, m.c, 
    rule=maxcooldown_rule,
    doc='Maximum cooling downshift')




```

</details>

<details>
<summary>Cooling load shift balance rule</summary>

Cooling load shifts must be compensated in defined time blocks, based on the pre-defined shifting period. Within a shifting period, the balance rule must be valid.
This guarantees that no general load increases or decreases emerge.


<img src="https://latex.codecogs.com/png.latex? \sum_{t}^{T^{shift}}{q^{coolshiftup}_{i,t}-q^{coolshiftdown}_{i,t}}=0" />



```


def coolshiftbalance_rule(m, t, c):
    expr=0
    if(data[c]["t_coolshift"]==0):
        return (m.q_coolshiftup[t*data[c]["t_coolshift"],c]-m.q_coolshiftdown[t*data[c]["t_coolshift"],c]==0)
    if((t*data[c]["t_coolshift"]>len(m.timesteps)-1)):
        return pyomo.Constraint.Skip
    for i in range(0, int(data[c]["t_coolshift"])):
        if((t*data[c]["t_coolshift"]+i)<len(m.timesteps)):
            expr+=m.q_coolshiftup[t*data[c]["t_coolshift"]+i,c]-m.q_coolshiftdown[t*data[c]["t_coolshift"]+i,c]
    return (expr==0)
m.coolshiftbalance = pyomo.Constraint(
    m.t, m.c, 
    rule=coolshiftbalance_rule,
    doc='Cool shift balance rule')



```

</details>

## Objective function

The model is implemented as a cost minimization. Thus, all emerging costs for all consumers over all timesteps are summarized in the objective function. The variable of the model are multiplied by the pre-defined cost parameters, which can either be values of timeseries. Costs emerge for electricity grid procurement, battery charging and discharging, thermal heat pump operation, district heat procurement, thermal storage charging and discharging, electric cooling operation, district cooling purchase and for emissions according to a CO2 price. Additionally, revenues can be generated from electricity grid feedin. For peak power costs, it must be differed wheather the peak powers from all consumers are multiplied with the power price or if the aggregated peak power is considered.


<img src="https://latex.codecogs.com/png.latex? z = min{\quad c^{tot}} " />

For non aggregated consumers, this results in the following objective function:


<img src="https://latex.codecogs.com/png.latex? c^{tot} = \sum_{i \in Consumers}{(\sum_{t \in T}{(q^{elgrid}_{i, t} + q^{elgrid}_{i, t} \cdot (\Pi^{elgrid,energy}_{i,t} + \Pi^{elgrid}_{i}) + (q^{batcharge}_{i, t} + q^{batdischarge}_{i, t}) \cdot C^{bat}_{i}+ q^{hp,th}_{i, t}  \cdot C^{hp}_{i}- q^{feedin}_{i, t}  \cdot \Pi^{feedin}_{i,t}+ \\ q^{dh}_{i, t}  \cdot \Pi^{dh}_{i,t}+ (q^{thstorecharge}_{i, t} + q^{thstoredischarge}_{i, t}) \cdot C^{thstore}_{i}+ q^{elcool,cool}_{i, t}  \cdot \Pi^{elcool}_{i}+ q^{dcool}_{i, t}  \cdot \Pi^{dcool}_{i,t}+ em^{total}_{i, t}  \cdot \Pi^{CO2}_{i,t}}) + p^{max}_{i}  \cdot \Pi^{power}_{i})}" />


If aggregated peak power is considered, the last term with peak power from the objective function with the peak power must be changed to aggregated peak power:

<img src="https://latex.codecogs.com/png.latex? c^{tot, agg} = \sum_{i \in Consumers}{(\sum_{t \in T}{(q^{elgrid}_{i, t} + q^{elgrid}_{i, t} \cdot (\Pi^{elgrid,energy}_{i,t} + \Pi^{elgrid}_{i}) + (q^{batcharge}_{i, t} + q^{batdischarge}_{i, t}) \cdot C^{bat}_{i}+ q^{hp,th}_{i, t}  \cdot C^{hp}_{i}- q^{feedin}_{i, t}  \cdot \Pi^{feedin}_{i,t}+ \\ q^{dh}_{i, t}  \cdot \Pi^{dh}_{i,t}+ (q^{thstorecharge}_{i, t} + q^{thstoredischarge}_{i, t}) \cdot C^{thstore}_{i}+ q^{elcool,cool}_{i, t}  \cdot \Pi^{elcool}_{i}+ q^{dcool}_{i, t}  \cdot \Pi^{dcool}_{i,t}+ em^{total}_{i, t}  \cdot \Pi^{CO2}_{i,t}}) )+ p^{max,agg}_{i}  \cdot \Pi^{power}}" />


By considering energy communities, the objective function must be adapted by the EEG sales and purchases. The energy price of the EEG purchase (which is equal to the sale value) is determined based on the mean value between electricity grid energy price and feedin tariff. The grid costs for EEG procurement are lowered by the pre-defined local and regional reduction factors from the input data.

<img src="https://latex.codecogs.com/png.latex? c^{tot, EEG} = c^{tot} + \sum_{t \in T}{( \sum_{i \in Consumers}{ ( \sum_{j \in Consumers}{( q^{c2L}_{i,j,t} \cdot (\frac{(\Pi^{elgrid,energy}_{i,t} + \Pi^{feedin}_{i,t} )}{2} + \Pi^{elgrid}_{i} \cdot F^{gridred}_{i,j})  + q^{L2c}_{i,j,t} \cdot \frac{(\Pi^{elgrid,energy}_{i,t} + \Pi^{feedin}_{i,t} )}{2} )} )} )}" />


Additionally, minor costs of 0.0001€/kWh are assumed for load shifts to prevent shifts in both directions at the same time step. These costs can be justified with minor effort that is required for performing load shifts. However, these costs can also be set to zero, but the total amount of shifted load should then not be considered.

<img src="https://latex.codecogs.com/png.latex? c^{tot, loadshift} = c^{tot,EEG} + \sum_{t \in T}{( \sum_{i \in Consumers}{( ( q^{elshiftup}_{i,t}+ q^{elshiftdown}_{i,t} + q^{heatshiftup}_{i,t} + q^{heatshiftdown}_{i,t}+q^{coolshiftup}_{i,t}} + q^{coolshiftdown}_{i,t})*0.0001))" />

```
def obj_costs_rule(m):
    """Calculate total costs by cost type.
    types: 'Invest', 'Fixed', 'Grid', 'Revenue'
    """       
    
    expr=      sum(m.q_elgrid[t, c]*(data[c]["C_elenergy"][t]+data[c]["C_elgrid"]) for t in m.t for c in m.c) \
                + sum((m.q_batdischarge[t, c] + m.q_batcharge[t, c])*(data[c]["C_bat"]) for t in m.t for c in m.c)  \
                + sum(m.q_HPth[t, c]*(data[c]["C_hp"]) for t in m.t for c in m.c) \
                - sum(m.q_feedin[t, c]*(data[c]["C_Feedin"][t]) for t in m.t for c in m.c)  \
                + sum(m.q_dh[t, c] * (data[c]["C_districtheat"][t]) for t in m.t for c in m.c) \
                + sum((m.q_thstoredischarge[t, c] + m.q_thstorecharge[t, c])*(data[c]["C_thstore"]) for t in m.t for c in m.c) \
                + sum(m.q_elCoolcool[t, c]*(data[c]["C_elcool"]) for t in m.t for c in m.c) \
                + sum(m.q_dcool[t, c] * (data[c]["C_districtcool"][t]) for t in m.t for c in m.c) \
                + sum(m.em_total[t, c] * (data[c]["P_CO2"][t]) for t in m.t for c in m.c)    
                
    if(energy_community):
        expr+= sum(m.q_c2L[t, c, j]*((data[c]["C_elenergy"][t]+data[c]["C_Feedin"][t])/2+data[c]["C_elgrid"]*data_EEG[c][j]) for t in m.t for c in m.c for j in m.j)
        expr-= sum(m.q_L2c[t, c, j]*((data[c]["C_elenergy"][t]+data[c]["C_Feedin"][t])/2) for t in m.t for c in m.c for j in m.j) 
        
    if(aggregate_peakpower):
        expr += m.p_max_agg*(data[consumers[0]]["C_elpower"])
    else:
        expr += sum(m.p_max[c]*(data[c]["C_elpower"]) for c in m.c)
        
        
    return expr

                
m.obj_costs = pyomo.Objective(
        rule=obj_costs_rule,
        sense=pyomo.minimize,
        doc='minimize(cost = sum of all cost)')



```

## Model solving

The model is solved by the solver. Default, gurobi is used as solver for the model. 

At the beginning of the model script, the solver is defined. This can be manually changed.

The model is the solved an the results are saved in the model.

```
# -------------------------------------------------------------------------
    # Solver definition
# -------------------------------------------------------------------------
solver="gurobi"

...

solver = SolverFactory(solver)

# Solve the model
results=solver.solve(m)
```

## Save results

Many different methods are defined in the [Load Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/load_data.py) script. The following methods are implemented.

<details>
<summary>load_results</summary>

This method loads the results of the timeseries in a dataframe with all time-dependent variables. Only peak power variables and EEG variables are excluded in this data frame.
The method requires a solved model as input. 

The data frame that is returned can be used for further result processing.

```

def load_results(m):
    model_vars = m.component_map(ctype=pyomo.Var)


    serieses = []   # collection to hold the converted "serieses"
    for k in model_vars.keys():   # this is a map of {name:pyo.Var}
    
        
        if("p_max" in str(k)):
            continue
        
        if("q_c2L" in str(k)):
            continue
        
        if("q_L2c" in str(k)):
            continue
        
        v = model_vars[k]
        
    
        # make a pd.Series from each    
        s = pd.Series(v.extract_values(), index=v.extract_values().keys())
    
        # if the series is multi-indexed we need to unstack it...
        if type(s.index[0]) == tuple:  # it is multi-indexed
            s = s.unstack(level=1)
        else:
            s = pd.DataFrame(s)         # force transition from Series -> df
        #print(s)
    
        # multi-index the columns
        s.columns = pd.MultiIndex.from_tuples([(k, t) for t in s.columns])
    
        serieses.append(s)
    
    df = pd.concat(serieses, axis=1)
    return df


```

</details>


<details>
<summary>load_results_peakpower</summary>

This method loads the results of the peak power variables. This includes the consumers' peak powers and aggregated peak powers.
The method requires a solved model as input. 

The data frame that is returned can be used for further result processing.

```

def load_results_peakpower(m):
    model_vars = m.component_map(ctype=pyomo.Var)


    serieses = []   # collection to hold the converted "serieses"
    for k in model_vars.keys():   # this is a map of {name:pyo.Var}
        
        if not("p_max" in str(k)):
            continue
        v = model_vars[k]
        
    
        # make a pd.Series from each    
        s = pd.Series(v.extract_values(), index=v.extract_values().keys())
    
        # if the series is multi-indexed we need to unstack it...
        if type(s.index[0]) == tuple:  # it is multi-indexed
            s = s.unstack(level=1)
        else:
            s = pd.DataFrame(s)         # force transition from Series -> df
        #print(s)
    
        # multi-index the columns
        s.columns = pd.MultiIndex.from_tuples([(k, t) for t in s.columns])
    
        serieses.append(s)
    
    df = pd.concat(serieses, axis=1)
    return df


```

</details>


<details>
<summary>load_results_trading</summary>

This method loads the results of the EEG trades, respectively the variables of the trades. Thus, it considers all sales and purchases by all consumers at all timesteps.
The method requires a solved model as input. 


```

def load_results_trading(m):
    model_vars = m.component_map(ctype=pyomo.Var)


    serieses = []   # collection to hold the converted "serieses"
    for k in model_vars.keys():   # this is a map of {name:pyo.Var}
        
        if not("q_c2L" in str(k) or "q_L2c" in str(k)):
            continue
        v = model_vars[k]
        
    
        # make a pd.Series from each    
        s = pd.Series(v.extract_values(), index=v.extract_values().keys())
    
        # if the series is multi-indexed we need to unstack it...
        if type(s.index[0]) == tuple:  # it is multi-indexed
            s = s.unstack(level=1)
        else:
            s = pd.DataFrame(s)         # force transition from Series -> df
        #print(s)
    
        # multi-index the columns
        s.columns = pd.MultiIndex.from_tuples([(k, t) for t in s.columns])
    
        serieses.append(s)
    
    df = pd.concat(serieses, axis=1)
    return df


```

The data frame must be further sorted for efficient processing. Therefore, the EEG sorting method is implemented which sorts the results of *load_results_trading* to data for further processing.

The method requires the data frame from *load_results_trading* and a list with all consumers as inputs

```
def sort_EEG(data, consumers):
    # Create an ExcelFile object
    
    #Empty dataframe
    df_total = pd.DataFrame()
    
    for consumer in consumers:
        results1 = data[("q_c2L", consumer)]
        
        for consumer1 in  consumers:
            results2 = results1.xs(consumer1, level=1).values
            #heading= "q_c2L_" + consumer.split('_')[1] + consumer1.split('_')[1]
            heading= ("q_c2L", consumer, consumer1)
            #results3 = np.insert(results2.astype(str), 0, heading)
            
            df_total[heading] = results2
            
    for consumer in consumers:
        results1 = data[("q_L2c", consumer)]
        
        for consumer1 in  consumers:
            results2 = results1.xs(consumer1, level=1).values
            #heading= "q_L2c_" + consumer.split('_')[1] + consumer1.split('_')[1]
            heading= ("q_L2c", consumer, consumer1)
            
            #results3 = np.insert(results2.astype(str), 0, heading)
            
            df_total[heading] = results2
            
            
    return df_total

```

</details>

The defined methods must be called in the main program of the model, which is done as follows:

```

# -------------------------------------------------------------------------
    # Get Results
# -------------------------------------------------------------------------

results_timeseries=load_data.load_results(m)
results_peakpower=load_data.load_results_peakpower(m)

if(energy_community):
    results_eeg=load_data.load_results_trading(m)
    results_eeg=load_data.sort_EEG(results_eeg, consumers)
else:
    results_eeg=pd.DataFrame()

```

## Save costs

Additionally to the results, the costs for all data frames are saved in separate data frames.

<details>
<summary>get_costs_timeseries</summary>

This method returns a data frame with costs for all time-dependent variables, respectively the timeseries data frame from *load_results*. Therefore, the timeseries data frame, [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx) with all costs and a list with consumers must be given to the model.

The data frame that is returned can be used for further result processing.

```

def get_costs_timeseries(df, input_data, consumers):
    df_new = pd.DataFrame()
    
    for consumer in consumers:
        df_new[("q_elgrid", consumer)]= df["q_elgrid"][consumer].values*(input_data[consumer]["C_elenergy"]+input_data[consumer]["C_elgrid"])
    
    for consumer in consumers:
        df_new[("q_feedin", consumer)]= df["q_feedin"][consumer].values*(-input_data[consumer]["C_Feedin"])
        
    for consumer in consumers:
        df_new[("q_eldemand", consumer)]= df["q_eldemand"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_pv", consumer)]= df["q_pv"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_batcharge", consumer)]= df["q_batcharge"][consumer].values*(input_data[consumer]["C_bat"])
        
    for consumer in consumers:
        df_new[("q_batdischarge", consumer)]= df["q_batdischarge"][consumer].values*(input_data[consumer]["C_bat"])
        
    for consumer in consumers:
        df_new[("q_batSOC", consumer)]= df["q_batSOC"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_heatdemand", consumer)]= df["q_heatdemand"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_HPel", consumer)]= df["q_HPel"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_HPth", consumer)]= df["q_HPth"][consumer].values*(input_data[consumer]["C_hp"])
        
    for consumer in consumers:
        df_new[("q_dh", consumer)]= df["q_dh"][consumer].values*(input_data[consumer]["C_districtheat"])
        
    for consumer in consumers:
        df_new[("q_thstorecharge", consumer)]= df["q_thstorecharge"][consumer].values*(input_data[consumer]["C_thstore"])
        
    for consumer in consumers:
        df_new[("q_thstoredischarge", consumer)]= df["q_thstoredischarge"][consumer].values*(input_data[consumer]["C_thstore"])
        
    for consumer in consumers:
        df_new[("q_thstoreSOC", consumer)]= df["q_thstoreSOC"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_cooldemand", consumer)]= df["q_cooldemand"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_elCoolel", consumer)]= df["q_elCoolel"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("q_elCoolcool", consumer)]= df["q_elCoolcool"][consumer].values*(input_data[consumer]["C_elcool"])
        
    for consumer in consumers:
        df_new[("q_dcool", consumer)]= df["q_dcool"][consumer].values*(input_data[consumer]["C_districtcool"])
        
    for consumer in consumers:
        df_new[("em_elgrid", consumer)]= df["em_elgrid"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("em_dh", consumer)]= df["em_dh"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("em_dcool", consumer)]= df["em_dcool"][consumer].values*(0)
        
    for consumer in consumers:
        df_new[("em_total", consumer)]= df["em_total"][consumer].values*(input_data[consumer]["P_CO2"])
        
    return df_new


```

</details>



<details>
<summary>get_costs_peakpower</summary>

This method returns a data frame with peak power costs, respectively the data frame from *load_results_peakpower*. Therefore, the peakpower data frame, [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx) with all costs, a list with consumers and the information wheather aggregated peak power pricing is implemented must be given to the model.

The data frame that is returned can be used for further result processing.

```

def get_costs_peakpower(df, input_data, consumers, aggregated):
    df_new = pd.DataFrame()
    
    if(aggregated):
        
        for consumer in consumers:
            df_new[("p_max", consumer)]= [0]
        
        df_new[("p_max_agg", "aggregated")]=df["p_max_agg"].values[-1]*input_data[consumers[0]]["C_elpower"]
        
    else:
    
        for consumer in consumers:
            df_new[("p_max", consumer)]= df["p_max"].loc[consumer]*input_data[consumer]["C_elpower"]
            
        df_new[("p_max_agg", "aggregated")]=[0]
    
    
        
    return df_new


```

</details>


<details>
<summary>get_costs_EEG</summary>

This method returns a data frame with EEG trading costs and revenues, respectively the sorted data frame from *load_results_trading*. Therefore, the trading data frame, [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx) with all costs, a list with consumers and the EEG reduction factors from the EEG sheet in the [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx) are required for the model.

The data frame that is returned can be used for further result processing.

```

def get_costs_EEG(df, input_data, consumers, input_data_EEG):
    
    df_new = pd.DataFrame()
    
    for consumer in consumers:
        for consumer1 in consumers:
            if(consumer != consumer1):
                df_new[("q_c2L", consumer, consumer1)] = df[("q_c2L", consumer, consumer1)]*((input_data[consumer]["C_elenergy"]+input_data[consumer]["C_Feedin"])/2+input_data[consumer]["C_elgrid"]*input_data_EEG[consumer][consumer1])
            
      
    for consumer in consumers:
        for consumer1 in consumers:
            if(consumer != consumer1):
                df_new[("q_L2c", consumer, consumer1)] = df[("q_L2c", consumer, consumer1)]*(-(input_data[consumer]["C_elenergy"]+input_data[consumer]["C_Feedin"])/2)  
      
    return df_new
        


```

</details>

The methods must be called in the model code as follows.

```

# -------------------------------------------------------------------------
    # Get costs
# -------------------------------------------------------------------------

costs_objective = pd.DataFrame({"Total costs" : [m.obj_costs.expr()]})
costs_timeseries = load_data.get_costs_timeseries(results_timeseries, data, consumers)
costs_power = load_data.get_costs_peakpower(results_peakpower, data, consumers, aggregate_peakpower)
if(energy_community):
    costs_eeg=load_data.get_costs_EEG(results_eeg, data, consumers, data_EEG)


```

## Save results and costs in Excel files

The model results and costs are saved into excel files. Therefore, a method is developed, which requires the scenario, filename of the output data, a dataframe list with results (including timeseries results, peak power results and optionally EEG results), information wheather energy communities are considered and cost results (including timeseries costs, peak power costs and optionally EEG costs and revenues) as inputs.

```

def data_to_excel(scenario, filename, df, energy_community, df_costs):
    if not os.path.exists(scenario):
        os.makedirs(scenario)
        
    file_path = scenario + '/' + filename

    with pd.ExcelWriter(file_path, engine='xlsxwriter') as writer:
        sheet_name = 'Timeseries'
        df[0].to_excel(writer, sheet_name=sheet_name, index=True)
        sheet_name = 'PeakPower'
        df[1].to_excel(writer, sheet_name=sheet_name, index=True)
        if energy_community:
            sheet_name = 'Community'
            df[2].to_excel(writer, sheet_name=sheet_name, index=True)
         
        sheet_name = 'Objective'
        df_costs[0].to_excel(writer, sheet_name=sheet_name, index=True)
        sheet_name = 'Costs_Timeseries'
        df_costs[1].to_excel(writer, sheet_name=sheet_name, index=True)
        sheet_name = 'Costs_PeakPower'
        df_costs[2].to_excel(writer, sheet_name=sheet_name, index=True)
        if energy_community:
            sheet_name = 'Costs_Community'
            df_costs[3].to_excel(writer, sheet_name=sheet_name, index=True)  
        
     
    string = "Data written"
    return string


```

The method is called in the model as follows.

```

# -------------------------------------------------------------------------
    # Results to excel
# -------------------------------------------------------------------------

results_excel = [results_timeseries, results_peakpower]
if energy_community:
    results_excel.append(results_eeg)
    
results_excel_costs = [costs_objective, costs_timeseries, costs_power]
if energy_community:
    results_excel_costs.append(costs_eeg)

load_data.data_to_excel(scenario, filename_out, results_excel, energy_community, results_excel_costs)

```




## Plot results and costs

The module [plots](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/plots.py) contains a variety of plot functions that can be used for model processing. The plots are saved in additionally generated directories in the directory of the scenario (depending on the scenario name in the [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)). The plots can be changed, extended or adapted for different investigations.


### Plot methods

The following plot methods are provided:

<details>
<summary>Electricity sankey</summary>

The electricity sankey provides sankey diagrams for the electricity balance rule of all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |   Timeseries results from *load_results* | 
| **results_eeg** |   EEG results from *load_results_trading* |
| **energy_community** |   Boolean value wheather EEGs are considered | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_electricity_sankey(*args, **kwargs):
    
    
    data_timeseries= kwargs.get("results_timeseries", False)
    data_eeg= kwargs.get("results_eeg", False)
    energy_community= kwargs.get("energy_community", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    consumer_target = consumers
    
    
    
    if not os.path.exists(scenario + "/balance_electricity"):
        os.makedirs(scenario + "/balance_electricity")
    
    for consumer in consumers:
        
        source=[]
        target=[]
        values=[]
        labels=[]
        color=[]
        
        
        # ------------- Inputs --------------
        #Electricity grid purchase
        source.append(0)
        target.append(1)
        values.append(np.sum(data_timeseries["q_elgrid"][consumer].values))
        labels.append("q_elgrid_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["q_elgrid"][consumer].values))) + "kWh")
        color.append("red")
        
        labels.append("")
        color.append("black")
        
        #PV
        source.append(2)
        target.append(1)
        values.append(np.sum(data_timeseries["q_pv"][consumer].values))
        labels.append("q_pv_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_pv"][consumer].values))) + "kWh")
        color.append("green")
        
        #Battery discharge
        source.append(3)
        target.append(1)
        values.append(np.sum(data_timeseries["q_batdischarge"][consumer].values))
        labels.append("q_batdischarge__" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_batdischarge"][consumer].values))) + "kWh")
        color.append("blue")
        
        
        
        # -------------- Outputs ---------------------------
        #Grid feedin
        source.append(1)
        target.append(4)
        values.append(np.sum(data_timeseries["q_feedin"][consumer].values))
        labels.append("q_feedin_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_feedin"][consumer].values)))+ "kWh")
        color.append("red")
        
        #Electricity demand
        source.append(1)
        target.append(5)
        values.append(np.sum(data_timeseries["q_eldemand"][consumer].values))
        labels.append("q_eldemand_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_eldemand"][consumer].values)))+ "kWh")
        color.append("yellow")
        
        #Battery charge
        source.append(1)
        target.append(6)
        values.append(np.sum(data_timeseries["q_batcharge"][consumer].values))
        labels.append("q_batcharge_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_batcharge"][consumer].values)))+ "kWh")
        color.append("blue")
        
        #Heat pump electricity
        source.append(1)
        target.append(7)
        values.append(np.sum(data_timeseries["q_HPel"][consumer].values))
        labels.append("q_HPel_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_HPel"][consumer].values)))+ "kWh")
        color.append("brown")
        
        #Electric cooling
        source.append(1)
        target.append(8)
        values.append(np.sum(data_timeseries["q_elCoolel"][consumer].values))
        labels.append("q_elCoolel_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_elCoolel"][consumer].values)))+ "kWh")
        color.append("darkblue")
        
        # -------------Community processes
        
        if(energy_community):
            i=9
            for consumer1 in consumer_target:
                if(consumer in consumer1):
                    continue
                
                #Purchase from consumers
                source.append(i)
                target.append(1)
                values.append(np.sum(data_eeg[('q_c2L', consumer, consumer1)].values))
                labels.append("q_c2L_" + str(consumer).split("_")[1] + "," +   str(consumer1).split("_")[1]  + ": " + str(round(np.sum(data_eeg[('q_c2L', consumer, consumer1)].values)))+ "kWh")
                color.append("lightseagreen")    
                
                i+=1
                
                #Sale to consumers
                source.append(1)
                target.append(i)
                values.append(np.sum(data_eeg[('q_L2c', consumer, consumer1)].values))
                labels.append("q_L2c_" + str(consumer).split("_")[1] + "," +   str(consumer1).split("_")[1]  + ": " + str(round(np.sum(data_eeg[('q_L2c', consumer, consumer1)].values)))+ "kWh")
                color.append("lightseagreen")    
                
    
                i+=1


        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = labels,
              color=color
            ),
            link = dict(
              source = source,
              target = target,
              value = values,
          ))])

        

        fig.update_layout(title_text="Electricity " + str(consumer), font_size=10)
        filename = scenario + "/balance_electricity" + "/electricity_" + str(consumer) + ".png"
        fig.show()
        fig.write_image(filename, scale=3)
        


```

</details>


<details>
<summary> Heat sankey</summary>

The heat sankey provides sankey diagrams for the heat balance rule of all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |   Timeseries results from *loadresults* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_heat_sankey(*args, **kwargs):
    
    
    data_timeseries= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")

    
    
    
    if not os.path.exists(scenario + "/balance_heat"):
        os.makedirs(scenario + "/balance_heat")
    
    for consumer in consumers:
        
        source=[]
        target=[]
        values=[]
        labels=[]
        color=[]
        
        # ------------- Inputs --------------
        #Heat pump input
        source.append(0)
        target.append(1)
        values.append(np.sum(data_timeseries["q_HPth"][consumer].values))
        labels.append("q_HPth_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["q_HPth"][consumer].values))) + "kWh")
        color.append("brown")
        
        labels.append("")
        color.append("black")
        
        #Heat storage discharge
        source.append(2)
        target.append(1)
        values.append(np.sum(data_timeseries["q_thstoredischarge"][consumer].values))
        labels.append("q_thstoredischarge_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_thstoredischarge"][consumer].values))) + "kWh")
        color.append("blue")
        
        #District heat
        source.append(3)
        target.append(1)
        values.append(np.sum(data_timeseries["q_dh"][consumer].values))
        labels.append("q_dh_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_dh"][consumer].values))) + "kWh")
        color.append("red")
        
        
        
        # -------------- Outputs ---------------------------
        #Thermal storage charge
        source.append(1)
        target.append(4)
        values.append(np.sum(data_timeseries["q_thstorecharge"][consumer].values))
        labels.append("q_thstorecharge_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_thstorecharge"][consumer].values)))+ "kWh")
        color.append("blue")
        
        #Heat demand
        source.append(1)
        target.append(5)
        values.append(np.sum(data_timeseries["q_heatdemand"][consumer].values))
        labels.append("q_heatdemand_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_heatdemand"][consumer].values)))+ "kWh")
        color.append("yellow")
        
        


        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = labels,
              color=color
            ),
            link = dict(
              source = source,
              target = target,
              value = values,
          ))])

        

        fig.update_layout(title_text="Heat " + str(consumer), font_size=10)
        filename = scenario + "/balance_heat" + "/heat_" + str(consumer) + ".png"
        fig.show()
        fig.write_image(filename, scale=3)
        


```

</details>


<details>
<summary> Cooling sankey</summary>

The Cooling sankey provides sankey diagrams for the cooling balance rule of all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |   Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_cooling_sankey(*args, **kwargs):
    
    
    data_timeseries= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")

    
    
    
    if not os.path.exists(scenario + "/balance_cooling"):
        os.makedirs(scenario + "/balance_cooling")
    
    for consumer in consumers:
        
        source=[]
        target=[]
        values=[]
        labels=[]
        color=[]
        
        # ------------- Inputs --------------
        #Electric cooling
        source.append(0)
        target.append(1)
        values.append(np.sum(data_timeseries["q_elCoolcool"][consumer].values))
        labels.append("q_elCoolcool_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["q_elCoolcool"][consumer].values))) + "kWh")
        color.append("brown")
        
        labels.append("")
        color.append("black")
        
        
        #District cooling
        source.append(2)
        target.append(1)
        values.append(np.sum(data_timeseries["q_dcool"][consumer].values))
        labels.append("q_dcool_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_dcool"][consumer].values))) + "kWh")
        color.append("red")
        
        
        
        # -------------- Outputs ---------------------------

        #Cooling demand
        source.append(1)
        target.append(3)
        values.append(np.sum(data_timeseries["q_cooldemand"][consumer].values))
        labels.append("q_cooldemand_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["q_cooldemand"][consumer].values)))+ "kWh")
        color.append("yellow")
        
        


        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = labels,
              color=color
            ),
            link = dict(
              source = source,
              target = target,
              value = values,
          ))])

        

        fig.update_layout(title_text="Cooling " + str(consumer), font_size=10)
        filename = scenario + "/balance_cooling" + "/cooling_" + str(consumer) + ".png"
        fig.show()
        fig.write_image(filename, scale=3)


```

</details>


<details>
<summary> Emissions sankey</summary>

The Emissions sankey provides sankey diagrams for the emission balance rule of all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |   Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_emission_sankey(*args, **kwargs):
    
    
    data_timeseries= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")

    
    
    
    if not os.path.exists(scenario + "/balance_emissions"):
        os.makedirs(scenario + "/balance_emissions")
    
    for consumer in consumers:
        
        source=[]
        target=[]
        values=[]
        labels=[]
        color=[]
        
        # ------------- Inputs --------------
        #Electricity grid emissions
        source.append(0)
        target.append(1)
        values.append(np.sum(data_timeseries["em_elgrid"][consumer].values))
        labels.append("em_elgrid_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["em_elgrid"][consumer].values))) + "kgCO2")
        color.append("red")
        
        labels.append("")
        color.append("black")
        
        
        #District heat grid emissions
        source.append(2)
        target.append(1)
        values.append(np.sum(data_timeseries["em_dh"][consumer].values))
        labels.append("em_dh_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["em_dh"][consumer].values))) + "kgCO2")
        color.append("green")
        
        #District cooling grid emissions
        source.append(3)
        target.append(1)
        values.append(np.sum(data_timeseries["em_dcool"][consumer].values))
        labels.append("em_dcool_" + str(consumer) + ": " + str(round(np.sum(data_timeseries["em_dcool"][consumer].values))) + "kgCO2")
        color.append("blue")
        
        
        # -------------- Outputs ---------------------------
        #Total emissions
        source.append(1)
        target.append(4)
        values.append(np.sum(data_timeseries["em_total"][consumer].values))
        labels.append("em_total_" + str(consumer)  + ": " + str(round(np.sum(data_timeseries["em_total"][consumer].values)))+ "kgCO2")
        color.append("brown")
        
        
        


        fig = go.Figure(data=[go.Sankey(
            node = dict(
              pad = 15,
              thickness = 20,
              line = dict(color = "black", width = 0.5),
              label = labels,
              color=color
            ),
            link = dict(
              source = source,
              target = target,
              value = values,
          ))])

        

        fig.update_layout(title_text="Emissions " + str(consumer), font_size=10)
        filename = scenario + "/balance_emissions" + "/emissions_" + str(consumer) + ".png"
        fig.show()
        fig.write_image(filename, scale=3)

```

</details>


<details>
<summary> Community heat map</summary>

The community heat map provides two heat maps for community sales and community purchases between all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_eeg** |   EEG trading results from *load_results_trading* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_community_heatmaps(*args, **kwargs):
    
    
    
    data_eeg= kwargs.get("results_eeg", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/trading"):
        os.makedirs(scenario + "/trading")
        
        
    df_c2L=pd.DataFrame(index=consumers)
    
    df_L2c=pd.DataFrame(index=consumers)
    
    consumers1=consumers
    
    for consumer in consumers:
        
        values_c2L=[]
        
        values_L2c=[]
        
        for consumer1 in consumers1:
            values_c2L.append(np.sum(data_eeg[('q_c2L', consumer, consumer1)].values))
            values_L2c.append(np.sum(data_eeg[('q_L2c', consumer, consumer1)].values))
            
        df_c2L[consumer] = values_c2L
        df_L2c[consumer] = values_L2c
        
    #Heat map c2L
    ax = sns.heatmap(df_c2L, linewidths=2, square=True, cmap='Blues', cbar_kws={'label': 'Traded electricity in kWh'}, annot=True, fmt='.0f')
    ax.set_title('Community purchase', fontsize = 12)
    plt.show()
    fig=ax.get_figure()
    fig.set_dpi(600)
    fig.set_size_inches(18.5, 10.5)
    filename = scenario + "/trading" + "/communitypurchase.png"
    fig.savefig(filename) 
    
    #Heat map cL2c
    ax = sns.heatmap(df_L2c, linewidths=2, square=True, cmap='Blues', cbar_kws={'label': 'Traded electricity in kWh'}, annot=True, fmt='.0f')
    ax.set_title('Community sale', fontsize = 12)
    plt.show()
    fig=ax.get_figure()
    fig.set_dpi(600)
    fig.set_size_inches(18.5, 10.5)
    filename = scenario + "/trading" + "/communitysale.png"
    fig.savefig(filename) 

```

</details>


<details>
<summary> Peak power bar</summary>

This method plots a bar chart with the electricity grid procurement peak power for all consumers, including aggregated peak power if it is considered. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_peakpower** |  Peak power results from *load_results_peakpower* | 
| **aggregate_peakpower** |   Boolean that specifies if aggregated peakpower is considered | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_peakpower_bar(*args, **kwargs):
    
    data_peakpower= kwargs.get("results_peakpower", False)
    scenario= kwargs.get("scenario", "Test")
    aggregated = kwargs.get("aggregate_peakpower", False)
    
    if not os.path.exists(scenario + "/peakpower"):
        os.makedirs(scenario + "/peakpower")
        
    ind = data_peakpower["p_max"].index.values
    ind=ind[:-1]
    
    
    ind = np.append(ind, "Aggregated")
        
    peakpower = data_peakpower["p_max"].values
    peakpower=peakpower[:-1]
        
    if(aggregated):
        peak_agg=data_peakpower["p_max_agg"].values[-1][0]
        peakpower=np.append(peakpower, peak_agg)
    else:
        peakpower=np.append(peakpower, 0)

    
    #Plot bar chart
    fig = plt.figure()
    fig.set_dpi(600)
    ax = fig.add_axes([0,0,1,1])
    ax.set_ylabel('Peak power in kW')
    ax.bar(ind, peakpower)
    #ax.legend(labels=[cols[0], cols[4]])
    plt.xticks(size = 8)
    filename = scenario + "/peakpower" + "/peakpower.png"
    fig.savefig(filename, bbox_inches='tight') 

```

</details>


<details>
<summary> Electricity grid combined load curve</summary>

This method plots a combined load curve for electricity grid procurement and feedin for all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_electricitygrid_combined_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_elgrid_combined"):
        os.makedirs(scenario + "/loadcurve_elgrid_combined")
        
    for consumer in consumers:
        df=data["q_elgrid"][consumer]
        df_feedin=data["q_feedin"][consumer]
        
        x=np.append(df.values, -df_feedin.values)
        ind=np.arange(len(x))
        
        plt.figure(dpi=600)
        plt.plot(ind, sorted(x, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('Electricity grid load in kWh')
        plt.title('Load curve electricity grid ' + consumer)
        filename = scenario + "/loadcurve_elgrid_combined" + "/elgrid_combined_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
```

</details>


<details>
<summary> Electricity grid procurement load curve</summary>

This method plots a load curve for electricity grid procurement for all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_electricitygrid_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_elgrid"):
        os.makedirs(scenario + "/loadcurve_elgrid")
        
    for consumer in consumers:
        df=data["q_elgrid"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('Electricity grid consumption in kWh')
        plt.title('Load curve electricity grid ' + consumer)
        filename = scenario + "/loadcurve_elgrid" + "/elgrid_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
```

</details>


<details>
<summary> Electricity grid feedin load curve</summary>

This method plots a load curve for electricity grid feedin for all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_electricityfeedin_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_elgridfeedin"):
        os.makedirs(scenario + "/loadcurve_elgridfeedin")
        
    for consumer in consumers:
        df=data["q_feedin"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('Electricity grid feedin in kWh')
        plt.title('Load curve electricity grid feedin ' + consumer)
        filename = scenario + "/loadcurve_elgridfeedin" + "/elgridfeedin_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
```

</details>


<details>
<summary> District heat grid procurement load curve</summary>

This method plots a load curve for district heat grid procurement for all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_districtheat_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_heat"):
        os.makedirs(scenario + "/loadcurve_heat")
        
    for consumer in consumers:
        df=data["q_dh"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('District heat purchase in kWh')
        plt.title('Load curve district heat purchase ' + consumer)
        filename = scenario + "/loadcurve_heat" + "/districtheat_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
```

</details>


<details>
<summary> District cooling grid procurement load curve</summary>

This method plots a load curve for district cooling grid procurement for all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_districtcool_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_cool"):
        os.makedirs(scenario + "/loadcurve_cool")
        
    for consumer in consumers:
        df=data["q_dcool"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('District cooling purchase in kWh')
        plt.title('Load curve district cooling purchase ' + consumer)
        filename = scenario + "/loadcurve_cool" + "/districtcooling_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
```

</details>


<details>
<summary> Total emissions load curve</summary>

This method plots a load curve for the total emissions by all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_emission_loadcurve(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/loadcurve_emissions"):
        os.makedirs(scenario + "/loadcurve_emissions")
        
    for consumer in consumers:
        df=data["em_total"][consumer]
        
        plt.figure(dpi=600)
        plt.plot(df.index.values, sorted(df.values, reverse=True))
        plt.xlabel('Time duration in h')
        plt.xticks(fontsize=8, rotation=0)
        plt.ylabel('Total emissions in kgCO2')
        plt.title('Load curve emissions ' + consumer)
        filename = scenario + "/loadcurve_emissions" + "/emissions_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
```

</details>


<details>
<summary> Battery time series</summary>

This method provides three plots with timeseries of the battery charging, discharging and SOC for all consumers' batteries. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_battery_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/battery"):
        os.makedirs(scenario + "/battery")
        
    for consumer in consumers:
        df_SOC=data["q_batSOC"][consumer]
        df_charge=data["q_batcharge"][consumer]
        df_discharge=data["q_batdischarge"][consumer]
        
        fig, axs = plt.subplots(3, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Battery ' + consumer, fontsize = 18)
        axs[0].plot(df_SOC.index.values, df_SOC.values)
        axs[0].set_ylabel('Battery SOC in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_SOC.index.values, df_charge.values)
        axs[1].set_ylabel('Battery charging kWh', color='k', fontsize=14)
        axs[1].xaxis.label.set_visible(False)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        axs[2].plot(df_SOC.index.values, [-x for x in df_discharge.values])
        axs[2].set_ylabel('Battery discharging kWh', color='k', fontsize=14)
        axs[2].set_xlabel('Time in h', color='k', fontsize=14)
        axs[2].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/battery" + "/battery_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
```

</details>


<details>
<summary> Thermal storage time series</summary>

This method provides three plots with timeseries of the thermal storage charging, discharging and SOC for all consumers' thermal storages. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_thstore_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/thstore"):
        os.makedirs(scenario + "/thstore")
        
    for consumer in consumers:
        df_SOC=data["q_thstoreSOC"][consumer]
        df_charge=data["q_thstorecharge"][consumer]
        df_discharge=data["q_thstoredischarge"][consumer]
        
        fig, axs = plt.subplots(3, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Thermal storage ' + consumer, fontsize = 18)
        axs[0].plot(df_SOC.index.values, df_SOC.values, 'red')
        axs[0].set_ylabel('ThStore SOC in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_SOC.index.values, df_charge.values, 'red')
        axs[1].set_ylabel('ThStore charging kWh', color='k', fontsize=14)
        axs[1].xaxis.label.set_visible(False)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        axs[2].plot(df_SOC.index.values,[-x for x in df_discharge.values], 'red')
        axs[2].set_ylabel('ThStore discharging kWh', color='k', fontsize=14)
        axs[2].set_xlabel('Time in h', color='k', fontsize=14)
        axs[2].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/thstore" + "/thstore_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
```

</details>


<details>
<summary> Electricity load shifting timeseries</summary>

This method provides timeseries for electricity load increase and decrease for the optimization period. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_eldemandshift_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/eldemandshift"):
        os.makedirs(scenario + "/eldemandshift")
        
    for consumer in consumers:
        df_elshiftup=data["q_elshiftup"][consumer]
        df_elshiftdown=data["q_elshiftdown"][consumer]
        
        fig, axs = plt.subplots(2, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Flexible electricity load ' + consumer, fontsize = 18)
        axs[0].plot(df_elshiftup.index.values, df_elshiftup.values, 'red')
        axs[0].set_ylabel('Load increase in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_elshiftdown.index.values,[-x for x in df_elshiftdown.values], 'red')
        axs[1].set_ylabel('Load decrease in kWh', color='k', fontsize=14)
        axs[1].set_xlabel('Time in h', color='k', fontsize=14)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/eldemandshift" + "/eldemandshift_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
```

</details>


<details>
<summary> Heat load shifting timeseries</summary>

This method provides timeseries for heat load increase and decrease for the optimization period. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_heatdemandshift_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/heatdemandshift"):
        os.makedirs(scenario + "/heatdemandshift")
        
    for consumer in consumers:
        df_heatshiftup=data["q_heatshiftup"][consumer]
        df_heatshiftdown=data["q_heatshiftdown"][consumer]
        
        fig, axs = plt.subplots(2, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Flexible heat load ' + consumer, fontsize = 18)
        axs[0].plot(df_heatshiftup.index.values, df_heatshiftup.values, 'green')
        axs[0].set_ylabel('Load increase in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_heatshiftdown.index.values,[-x for x in df_heatshiftdown.values], 'green')
        axs[1].set_ylabel('Load decrease in kWh', color='k', fontsize=14)
        axs[1].set_xlabel('Time in h', color='k', fontsize=14)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/heatdemandshift" + "/heatdemandshift_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
```

</details>

<details>
<summary> Cooling load shifting timeseries</summary>

This method provides timeseries for cooling load increase and decrease for the optimization period. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_cooldemandshift_timeseries(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/cooldemandshift"):
        os.makedirs(scenario + "/cooldemandshift")
        
    for consumer in consumers:
        df_coolshiftup=data["q_coolshiftup"][consumer]
        df_coolshiftdown=data["q_coolshiftdown"][consumer]
        
        fig, axs = plt.subplots(2, sharex=True)
        fig.set_dpi(600)
        fig.set_size_inches(18.5, 10.5)
        fig.suptitle('Flexible cooling load ' + consumer, fontsize = 18)
        axs[0].plot(df_coolshiftup.index.values, df_coolshiftup.values, 'blue')
        axs[0].set_ylabel('Load increase in kWh', color='k', fontsize=14)
        axs[0].xaxis.label.set_visible(False)
        axs[0].tick_params(axis='both', which='major', labelsize=12)
        axs[1].plot(df_coolshiftdown.index.values,[-x for x in df_coolshiftdown.values], 'blue')
        axs[1].set_ylabel('Load decrease in kWh', color='k', fontsize=14)
        axs[1].set_xlabel('Time in h', color='k', fontsize=14)
        axs[1].tick_params(axis='both', which='major', labelsize=12)
        filename = scenario + "/cooldemandshift" + "/cooldemandshift_" + consumer + ".png"
        plt.savefig(filename) 
        plt.show()
        
```

</details>


<details>
<summary> Total load shifting bars</summary>

This method provides bar charts for the total load shifted in the electricity, heating and cooling sector. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **results_timeseries** |  Timeseries results from *load_results* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_demandshift_bar(*args, **kwargs):
    
    data= kwargs.get("results_timeseries", False)
    consumers= kwargs.get("consumers", [])
    scenario= kwargs.get("scenario", "Test")
    
    if not os.path.exists(scenario + "/demandshift"):
        os.makedirs(scenario + "/demandshift")
        
    for consumer in consumers:
        df_elshiftup=data["q_elshiftup"][consumer]
        df_heatshiftup=data["q_heatshiftup"][consumer]
        df_coolshiftup=data["q_coolshiftup"][consumer]
        
        elec=np.sum(df_elshiftup.values)
        heat=np.sum(df_heatshiftup.values)
        cool=np.sum(df_coolshiftup.values)

        
        #Plot bar chart
        fig = plt.figure()
        fig.set_dpi(600)
        ax = fig.add_axes([0,0,1,1])
        ax.set_ylabel('Demand shift in kW')
        ax.bar(["Electricity", "Heating", "Cooling"], [elec, heat, cool], color=["red", "green", "blue"])
        plt.xticks(size = 8)
        filename = scenario + "/demandshift" + "/costs_peakpower_" + consumer + ".png"
        fig.savefig(filename, bbox_inches='tight') 
        
```

</details>


<details>
<summary> Peak power bar</summary>

This method gives peak power costs bar charts for all consumers and the aggregated peak power. In the costs it is already considered if aggregated or non-aggregated peak power is considered. Therefore, no additional specification is necessary. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **costs_power** |  Timeseries results from *get_costs_peakpower* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_costs_peakpower_bar(*args, **kwargs):
    
    costs_peakpower= kwargs.get("costs_power", False)
    scenario= kwargs.get("scenario", "Test")
    consumers= kwargs.get("consumers", False)
    
    consumers.append("Aggregated")

    
    if not os.path.exists(scenario + "/costs_peakpower"):
        os.makedirs(scenario + "/costs_peakpower")
        

    #Plot bar chart
    fig = plt.figure()
    fig.set_dpi(600)
    ax = fig.add_axes([0,0,1,1])
    ax.set_ylabel('Peak power costs in €')
    ax.bar(consumers, costs_peakpower.values[0])
    plt.xticks(size = 8)
    filename = scenario + "/costs_peakpower" + "/costs_peakpower.png"
    fig.savefig(filename, bbox_inches='tight') 
    
    consumers.remove("Aggregated")
```

</details>


<details>
<summary> Community bar charts</summary>

This method provides bar charts for community trading costs and revenues for trades between all consumers. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **costs_eeg** |  EEG cost results from *get_costs_EEG* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_costs_community_bar(*args, **kwargs):
    
    costs_eeg= kwargs.get("costs_eeg", False)
    scenario= kwargs.get("scenario", "Test")
    consumers= kwargs.get("consumers", False)
    
    consumers1=consumers
    
    if not os.path.exists(scenario + "/costs_community"):
        os.makedirs(scenario + "/costs_community")

    for consumer in consumers:
        
        buy=[]
        sale=[]
        lbl_buy=[]
        lbl_sale=[]
        
        for consumer1 in consumers1:
            
            if consumer==consumer1:
                continue
            
            buy.append(np.sum(costs_eeg[("q_c2L", consumer, consumer1)].values))
            sale.append(np.sum(costs_eeg[("q_L2c", consumer, consumer1)].values))
            lbl_buy.append("Buy_(" + consumer.split('_')[1] + "," + consumer1.split('_')[1] + ")")
            lbl_sale.append("Sale_(" + consumer.split('_')[1] + "," + consumer1.split('_')[1] + ")")
            
            
        #Plot bar chart
        fig = plt.figure()
        fig.set_dpi(600)
        ax = fig.add_axes([0,0,1,1])
        plt.axhline(0, color='black', linewidth=1)
        ax.set_ylabel('Community costs ' + consumer + ' in €')  
        ax.bar(np.append(lbl_buy, lbl_sale), np.append(buy, sale))
        plt.xticks(size = 8)
        filename = scenario + "/costs_community" + "/costs_community_" + consumer + ".png"
        fig.savefig(filename, bbox_inches='tight') 
```

</details>


<details>
<summary> Total cost bar charts</summary>

This method provides bar charts for all costs for all consumers and the total costs for each consumer. Additionally, the total consumer costs and total model costs are presented in a separate bar chart. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **costs_timeseries** |  Timeseries cost results from *get_costs* |
| **costs_eeg** |  EEG cost results from *get_costs_EEG* |  
| **costs_power** |  Timeseries results from *get_costs_peakpower* | 
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 

```

def get_costs_total(*args, **kwargs):
    
    costs_timeseries= kwargs.get("costs_timeseries", False)
    costs_eeg= kwargs.get("costs_eeg", False)
    costs_power= kwargs.get("costs_power", False)
    scenario= kwargs.get("scenario", "Test")
    consumers= kwargs.get("consumers", False)
    
    consumers1=consumers
    
    if not os.path.exists(scenario + "/costs_total"):
        os.makedirs(scenario + "/costs_total")
        
        
    
    tot_labels=[]
    tot_values=[]
    
    for consumer in consumers:
        
        consumer_labels=[]
        consumer_values=[]
        
        # ----- Costs timeseries ----------
        
        #Iterate over all values
        for col in costs_timeseries.columns:
            
            #If consumer in column, save the value
            if(str(consumer) in str(col)):
                
                data=np.sum(costs_timeseries[col])
                
                #But only if inequal to zero
                if(data!=0):
                    if("em_total" in str(col)):
                        consumer_labels.append("em")
                    elif("thstorecharge" in str(col)):
                        consumer_labels.append("thcharge")
                    elif("thstoredischarge" in str(col)):
                        consumer_labels.append("thdischarge")
                    else:
                        consumer_labels.append(col[0].split('_')[1])
                    consumer_values.append(data)
                    
                    
        # ----- Costs eeg --------
        
        buy=[]
        sale=[]
        for consumer1 in consumers1:
           
            if(consumer1 != consumer):
                buy.append(np.sum(costs_eeg[("q_c2L", consumer, consumer1)].values))
                sale.append(np.sum(costs_eeg[("q_L2c", consumer, consumer1)].values))
           
           
        #Append community costs to consumers
        consumer_labels.append("Com Purchase")
        consumer_values.append(np.sum(buy))
        consumer_labels.append("Com Sale")
        consumer_values.append(np.sum(sale))
        
        
        # ------ Costs peakpower -----------
        
        c_power = costs_power[("p_max", consumer)].values[0]
        
        if(c_power!=0):
            consumer_labels.append("P_peak")
            consumer_values.append(c_power)
        
           
        #Append total consumer costs            
        consumer_labels.append("Total")
        total=np.sum(consumer_values)
        consumer_values.append(total)
        
        tot_labels.append(consumer)
        tot_values.append(total)
        
        #Plot bar chart
        fig = plt.figure()
        fig.set_dpi(600)
        fig.suptitle('Costs ' + consumer, fontsize = 12)
        ax = fig.add_axes([0,0,1,1])
        plt.axhline(0, color='black', linewidth=1)
        ax.set_ylabel('Costs in €')  
        ax.bar(consumer_labels, consumer_values)
        plt.xticks(size = 5)
        filename = scenario + "/costs_total" + "/costs_total_" + consumer + ".png"
        fig.savefig(filename, bbox_inches='tight') 
        
        
    #Sum total values
    tot_labels.append("Total")
    tot_values.append(np.sum(tot_values))
    
    
    #Plot bar chart total
    fig = plt.figure()
    fig.set_dpi(600)
    fig.suptitle('Total costs', fontsize = 12)
    ax = fig.add_axes([0,0,1,1])
    plt.axhline(0, color='black', linewidth=1)
    ax.set_ylabel('Costs in €')  
    ax.bar(tot_labels, tot_values)
    plt.xticks(size = 8)
    filename = scenario + "/costs_total" + "/costs_total.png"
    fig.savefig(filename, bbox_inches='tight') 
        
```

</details>


<details>
<summary> Key performance indicators (KPI)</summary>

This method presents defined KPIs as bar charts and also saves them in an excel file. Defined KPIs are *Total costs*, *Total emissions*, *Electricity grid procurement peak power*, *Total electricity grid consumption*, *Total electricity grid feedin*, *Total district heat consumption*, *Total district cooling grid consumption* and *Total traded electricity in the EEG*. The following parameters must be set in the method:

| **Parameter** | **Description** | 
| ------ | ------ | 
| **costs_objective** |  Total costs of the model |
| **consumers** |   List with all consumers | 
| **scenario** |   Scenario name from  [Input Data](https://gitlab.com/team-ensys/renvelope_model/-/blob/main/Input_Data.xlsx)| 
| **results_timeseries** |  Timeseries results from *load_data* |
| **results_peakpower** |  Peak power results from *load_data_peakpower* |
| **results_eeg** |  Trading results from *load_data_trading* |
| **aggregate_peakpower** |  Aggregate peak power boolean |
| **energy_community** |  EEG consideration boolean |

```

def get_KPI(*args, **kwargs):
    
    costs_objective= kwargs.get("costs_objective", False)
    scenario= kwargs.get("scenario", "Test")
    consumers= kwargs.get("consumers", False)
    results_timeseries= kwargs.get("results_timeseries", False)
    results_peakpower= kwargs.get("results_peakpower", False)
    results_eeg= kwargs.get("results_eeg", False)
    aggregate_peakpower= kwargs.get("aggregate_peakpower", False)
    energy_community= kwargs.get("energy_community", False)
    
    consumers1=consumers
    
    if not os.path.exists(scenario + "/KPI"):
        os.makedirs(scenario + "/KPI")
        
    #KPI list
    kpi_lbl=[]
    kpi_val=[]

    #Total costs
    kpi_lbl.append("Total costs in €")
    kpi_val.append(costs_objective.values[0][0])
    
    emissions=[]
    for consumer in consumers:
        
        #Add emissions of consumers to list
        emissions.append(np.sum(results_timeseries["em_total"][consumer].values))
    
    #Append emissions to struct
    kpi_lbl.append("Total emissions in kg")
    kpi_val.append(np.sum(emissions))
    
    #Peak power KPI
    kpi_lbl.append("Electricity grid peak power in kW")
    if aggregate_peakpower:
        kpi_val.append(results_peakpower["p_max_agg"].values[-1][0])
    else:
        kpi_val.append(np.sum(np.nan_to_num(results_peakpower["p_max"].values)))
        
        
    #Electricity grid procurement and feedin, district heating and district cooling grid
    elgrid=[]
    feedin=[]
    dhgrid=[]
    dcool=[]
    for consumer in consumers:
        
        #Add emissions of consumers to list
        elgrid.append(np.sum(results_timeseries["q_elgrid"][consumer].values))
        feedin.append(np.sum(results_timeseries["q_feedin"][consumer].values))
        dhgrid.append(np.sum(results_timeseries["q_dh"][consumer].values))
        dcool.append(np.sum(results_timeseries["q_dcool"][consumer].values))
    
    #Append values to struct
    kpi_lbl.append("Electricity grid consumption in kWh")
    kpi_val.append(np.sum(elgrid))
    
    kpi_lbl.append("Electricity grid feedin in kWh")
    kpi_val.append(np.sum(feedin))
    
    kpi_lbl.append("District heat consumption in kWh")
    kpi_val.append(np.sum(dhgrid))
    
    kpi_lbl.append("District cooling consumption in kWh")
    kpi_val.append(np.sum(dcool))
    
    
    #Community trading
    values_c2L=[]
    if energy_community:
        for consumer in consumers:
            for consumer1 in consumers1:
                values_c2L.append(np.sum(results_eeg[('q_c2L', consumer, consumer1)].values))

    kpi_lbl.append("Traded electricity in kWh")
    kpi_val.append(np.sum(values_c2L))
    
    #Plot the KPIs
    fig, axes = plt.subplots(4, 2, figsize=(10, 12))

    # Plot on each subplot
    k=0
    for i in range(4):
        for j in range(2):
            ax = axes[i, j]
            ax.bar([kpi_lbl[k]], [kpi_val[k]])
            ax.set_xlabel('KPI')
            ax.set_ylabel(round(kpi_val[k], 2))
            k+=1
    
    # Adjust spacing between subplots
    plt.tight_layout()
    
    # Display the plot
    plt.show()
    
    #Save the plot
    filename = scenario + "/KPI" + "/KPI.png"
    fig.savefig(filename, bbox_inches='tight') 
    
    
    #Write KPI results to excel
    df=pd.DataFrame(index=kpi_lbl)
    df["Values"] = kpi_val
    filename = scenario + "/KPI" + "/KPI.xlsx"
    with pd.ExcelWriter(filename, engine='xlsxwriter') as writer:
        sheet_name = 'KPI'
        df.to_excel(writer, sheet_name=sheet_name, index=True)
```

</details>


### Call of the plots

The call of the plot functions in the code is done as follows:


<details>
<summary> Plots (KPI)</summary>

```
# -------------------------------------------------------------------------
    # Plot results
# -------------------------------------------------------------------------

#Electricity balance sankey for all consumers
plots.get_electricity_sankey(results_timeseries=results_timeseries, results_eeg=results_eeg, energy_community=energy_community, consumers=consumers, scenario=scenario)

#Heat balance sankey for all consumers
plots.get_heat_sankey(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Cooling balance sankey for all consumers
plots.get_cooling_sankey(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Emission balance sankey for all consumers
plots.get_emission_sankey(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Community heatmaps
if(energy_community):
   plots.get_community_heatmaps(results_eeg=results_eeg, consumers=consumers, scenario=scenario) 
   
#Peak power results
plots.get_peakpower_bar(results_peakpower=results_peakpower, aggregate_peakpower=aggregate_peakpower, scenario=scenario)

#Load curve electricity grid
plots.get_electricitygrid_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve electricity grid feedin
plots.get_electricityfeedin_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve combined
plots.get_electricitygrid_combined_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve district heat grid
plots.get_districtheat_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve district cooling grid
plots.get_districtcool_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve emissions
plots.get_emission_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Battery timeseries
plots.get_battery_timeseries(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Thermal storage timeseries
plots.get_thstore_timeseries(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)


# -------------------------------------------------------------------------
    # Plot costs
# -------------------------------------------------------------------------
#Peak power costs
plots.get_costs_peakpower_bar(costs_power=costs_power, scenario=scenario, consumers=consumers)


#Community costs
if(energy_community):
    plots.get_costs_community_bar(costs_eeg=costs_eeg, scenario=scenario, consumers=consumers)

#Total costs


# -------------------------------------------------------------------------
    # KPI
# -------------------------------------------------------------------------
#Plot of the KPIs and write KPI to excel --> more recommended to write KPIs in Table
plots.get_KPI(costs_objective=costs_objective, scenario=scenario, consumers=consumers, 
              results_timeseries=results_timeseries, results_peakpower=results_peakpower, results_eeg=results_eeg, 
              aggregate_peakpower=aggregate_peakpower, energy_community=energy_community)

```

</details>
