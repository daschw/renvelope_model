# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 09:42:45 2023

@author: Matthias Maldet
"""
import pyomo.environ
import load_data
import pyomo.core as pyomo
from datetime import datetime
from pyomo.opt import SolverFactory
import pandas as pd
import plots as plots


# -------------------------------------------------------------------------
    # Solver definition
# -------------------------------------------------------------------------
solver="gurobi"

# -------------------------------------------------------------------------
    # Input and output files
# -------------------------------------------------------------------------


#Filename of Input Data
filename="Input_Data.xlsx"



#Data output name
filename_out = "results.xlsx"

# -------------------------------------------------------------------------
    # Load scenarios --> iterating over scenarios would be possible with adaption of Code structure
# -------------------------------------------------------------------------


df_scenario = load_data.load_scenario(filename)

#Define if energy community operation is activated
energy_community=df_scenario["Energy_community"].values[0]

#Define if peak power should be aggregated
aggregate_peakpower=df_scenario["Aggregate_Peakpower"].values[0]

#Variable if test run is true
test=df_scenario["Test"].values[0]

#Scenario name
scenario = df_scenario["Scenario"].values[0]

#Resolution of the data
deltaT=df_scenario["Resolution"].values[0]

# -------------------------------------------------------------------------


#Load the Input data
data=load_data.load_data(filename, test)
print("--------- Input data loaded -------------- \n")


#Load the EEG data
if(energy_community):
    data_EEG=load_data.load_data_EEG(filename)
    print("--------- Input data EEG loaded -------------- \n")

#Data keys = consumers
consumers=list(data.keys())



# -------------------------------------------------------------------------
    # create model
# -------------------------------------------------------------------------
m = pyomo.ConcreteModel()
m.name = 'Renvelope_Model'
m.created = datetime.now().strftime('%Y%m%dT%H%M')



# -------------------------------------------------------------------------
    # define sets
# -------------------------------------------------------------------------


#Timesteps based on timeseries - PV was taken, but also possible with other timeseries
m.timesteps = list(range(len(data[consumers[0]]["PV"])))

#Set of timesteps
m.t = pyomo.Set(
        initialize=m.timesteps,
        ordered=True,
        doc='Set of timesteps')

#Set of consumers
m.c = pyomo.Set(
        initialize=consumers,
        ordered=True,
        doc='Set of consumers')

#Set of consumers to trade
m.j = pyomo.Set(
        initialize=consumers,
        ordered=True,
        doc='Set of consumers to trade')


# -------------------------------------------------------------------------
    # define variables
# -------------------------------------------------------------------------

#Electricity grid
m.q_elgrid = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elgrid",
        doc='Electricitygrid Procurement')

m.q_feedin = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_feedin",
        doc='Electricitygrid Feedin')

m.p_max = pyomo.Var(
        m.c,
        within=pyomo.NonNegativeReals,
        name="p_max",
        doc='Electricitygrid maximum power')


#Variable Electricity demand for potential extensions
m.q_eldemand = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_eldemand",
        doc='Variable electricity demand')


#PV generation for potential future extensions
m.q_pv = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_PV",
        doc='Variable PV generation')


#Battery
m.q_batcharge = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_batcharge",
        doc='Battery Charging')

m.q_batdischarge = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_batdischarge",
        doc='Battery Discharging')

m.q_batSOC = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_batSOC",
        doc='Battery SOC')


#Variable Heat demand for potential extensions
m.q_heatdemand = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_heatdemand",
        doc='Variable heat demand')

#Heat pump
m.q_HPel = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_HPel",
        doc='Heat pump electricity')

m.q_HPth = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_HPth",
        doc='Heat pump heat')

#District heat

m.q_dh = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_dh",
        doc='District heat energy')

#Thermal Storage
m.q_thstorecharge = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_thstorecharge",
        doc='Thermal Storage Charging')

m.q_thstoredischarge = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_thstoredischarge",
        doc='Thermal Storage Discharging')

m.q_thstoreSOC = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_thstoreSOC",
        doc='Thermal Storage SOC')


#Variable Cooling demand for potential extensions
m.q_cooldemand = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_cooldemand",
        doc='Variable cooling demand')

#Electric cooling
m.q_elCoolel = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elCoolel",
        doc='Electric cooling with heat pump - Electricity input')

m.q_elCoolcool = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elCoolcool",
        doc='Electric cooling with heat pump - Cooling output')

#District cooling

m.q_dcool = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_dcool",
        doc='District cooling energy')

#Emissions electricity grid
m.em_elgrid = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="em_elgrid",
        doc='CO2 emissions electricity grid')

#Emissions district heating grid
m.em_dh = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="em_dh",
        doc='CO2 emissions district heating grid')

#Emissions district cooling grid
m.em_dcool = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="em_dcool",
        doc='CO2 emissions district cooling grid')

#Total emissions
m.em_total = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="em_total",
        doc='Total CO2 emissions')

#EEG Extension

m.q_c2L = pyomo.Var(
        m.t, m.c, m.j,
        within=pyomo.NonNegativeReals,
        name="q_c2L",
        doc='EEG Purchase')

m.q_L2c = pyomo.Var(
        m.t, m.c, m.j,
        within=pyomo.NonNegativeReals,
        name="q_L2c",
        doc='EEG Sale')

#Aggregated peak power
m.p_max_agg = pyomo.Var(
        within=pyomo.NonNegativeReals,
        name="p_max_agg",
        doc='Electricitygrid maximum power aggregated')


#Extension load shift variables
#Electricity load shift up
m.q_elshiftup = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elshiftup",
        doc='Electricity load upshift')

#Electricity load down shift
m.q_elshiftdown = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_elshiftdown",
        doc='Electricity load downshift')


#Heat load shift up
m.q_heatshiftup = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_heatshiftup",
        doc='Heat load upshift')

#Heat load down shift
m.q_heatshiftdown = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_heatshiftdown",
        doc='Heat load downshift')


#Cooling load shift up
m.q_coolshiftup = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_coolshiftup",
        doc='Cooling load upshift')

#Heat load down shift
m.q_coolshiftdown = pyomo.Var(
        m.t, m.c,
        within=pyomo.NonNegativeReals,
        name="q_coolshiftdown",
        doc='Cooling load downshift')


# -------------------------------------------------------------------------
    # constraints
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
def elecdemand_rule(m, t, c):
    return (m.q_eldemand[t, c] 
            == data[c]["D_el"][t])
m.elecdemand = pyomo.Constraint(
    m.t, m.c, 
    rule=elecdemand_rule,
    doc='Electricity demand rule')

# -------------------------------------------------------------------------
def pv_rule(m, t, c):
    return (m.q_pv[t, c] 
            == data[c]["PV"][t]*data[c]["P_pv"])
m.pv = pyomo.Constraint(
    m.t, m.c, 
    rule=pv_rule,
    doc='PV generation rule')

# -------------------------------------------------------------------------
def maxgrid_rule(m, t, c):
    return (m.q_elgrid[t, c] 
            <= data[c]["P_elgrid"]*deltaT)
m.maxgrid = pyomo.Constraint(
    m.t, m.c, 
    rule=maxgrid_rule,
    doc='Maximum grid procurement power')

# -------------------------------------------------------------------------
def maxfeedin_rule(m, t, c):
    return (m.q_feedin[t, c] 
            <= data[c]["P_elgrid"]*deltaT)
m.maxfeedin = pyomo.Constraint(
    m.t, m.c, 
    rule=maxfeedin_rule,
    doc='Maximum grid feedin')

# -------------------------------------------------------------------------
def maxcharge_rule(m, t, c):
    return (m.q_batcharge[t, c] 
            <= data[c]["P_bat"]*deltaT)
m.maxcharge = pyomo.Constraint(
    m.t, m.c, 
    rule=maxcharge_rule,
    doc='Maximum battery charge power')

# -------------------------------------------------------------------------
def maxdischarge_rule(m, t, c):
    return (m.q_batdischarge[t, c] 
            <= data[c]["P_bat"]*deltaT)
m.maxdischarge = pyomo.Constraint(
    m.t, m.c, 
    rule=maxdischarge_rule,
    doc='Maximum battery discharge power')

# -------------------------------------------------------------------------
def batterybalance_rule(m, t, c):
    if t == m.timesteps[0]: 
            return pyomo.Constraint.Skip
    else:
        return (m.q_batSOC[t,c] == m.q_batSOC[t-1, c] * data[c]["Eta_batSB"] + m.q_batcharge[t, c] * data[c]["Eta_bat"] - m.q_batdischarge[t, c] / data[c]["Eta_bat"])
m.batterybalance = pyomo.Constraint(
    m.t, m.c, 
    rule=batterybalance_rule,
    doc='Battery balance')

# -------------------------------------------------------------------------
def sto_initial_and_end_rule(m, c): 
    return (m.q_batSOC[0,c]
            == m.q_batSOC[m.timesteps[-1], c])
m.sto_initial_and_end = pyomo.Constraint(
    m.c,
    rule=sto_initial_and_end_rule,
    doc='storage content initial == end')

# -------------------------------------------------------------------------
def sto_initial_rule(m, c): 
        return (m.q_batSOC[m.timesteps[0], c] == 0)
m.sto_initial = pyomo.Constraint(
   m.c,
   rule=sto_initial_rule,
   doc='storage content initial == 0')


def discharge_initial_rule(m, c): 
        return (m.q_batdischarge[m.timesteps[0], c] <= m.q_batSOC[m.timesteps[0], c])
m.discharge_initial = pyomo.Constraint(
   m.c,
   rule=discharge_initial_rule,
   doc='Discharge initial <= Storage Initial ')



# -------------------------------------------------------------------------
def sto_max1_rule(m, t, c): 
    return (m.q_batSOC[t, c] \
            <= (data[c]["SOC_bat"]))
m.sto_max1 = pyomo.Constraint(
    m.t, m.c,
    rule=sto_max1_rule,
    doc='storage soc <= max storage soc')

# -------------------------------------------------------------------------
def maxchargethstore_rule(m, t, c):
    return (m.q_thstorecharge[t, c] 
            <= data[c]["P_thstore"]*deltaT)
m.maxchargethstore = pyomo.Constraint(
    m.t, m.c, 
    rule=maxchargethstore_rule,
    doc='Maximum Thermal Storage charge power')

# -------------------------------------------------------------------------
def maxdischargethstore_rule(m, t, c):
    return (m.q_thstoredischarge[t, c] 
            <= data[c]["P_thstore"]*deltaT)
m.maxdischargethstore = pyomo.Constraint(
    m.t, m.c, 
    rule=maxdischargethstore_rule,
    doc='Maximum Thermal Storage discharge power')

# -------------------------------------------------------------------------
def thstorebalance_rule(m, t, c):
    if t == m.timesteps[0]: 
            return pyomo.Constraint.Skip
    else:
        return (m.q_thstoreSOC[t,c] == m.q_thstoreSOC[t-1, c] * data[c]["Eta_thstoreSB"] + m.q_thstorecharge[t, c] * data[c]["Eta_thstore"] - m.q_thstoredischarge[t, c] / data[c]["Eta_thstore"])
m.thstorebalance = pyomo.Constraint(
    m.t, m.c, 
    rule=thstorebalance_rule,
    doc='Thermal storage balance')

# -------------------------------------------------------------------------
def thstore_initial_and_end_rule(m, c): 
    return (m.q_thstoreSOC[0,c]
            == m.q_thstoreSOC[m.timesteps[-1], c])
m.thstore_initial_and_end = pyomo.Constraint(
    m.c,
    rule=thstore_initial_and_end_rule,
    doc='Thermal storage content initial == end')

# -------------------------------------------------------------------------
def thstore_initial_rule(m, c): 
        return (m.q_thstoreSOC[m.timesteps[0], c] == 0)
m.thstore_initial = pyomo.Constraint(
   m.c,
   rule=thstore_initial_rule,
   doc='Thermal storage content initial == 0')


def thstore_discharge_initial_rule(m, c): 
        return (m.q_thstoredischarge[m.timesteps[0], c] <= m.q_thstoreSOC[m.timesteps[0], c])
m.thstore_discharge_initial = pyomo.Constraint(
   m.c,
   rule=thstore_discharge_initial_rule,
   doc='Thermal storage Discharge initial <= Thermal Storage Initial ')



# -------------------------------------------------------------------------
def thstore_max1_rule(m, t, c): 
    return (m.q_thstoreSOC[t, c] \
            <= (data[c]["SOC_thstore"]))
m.thstore_max1 = pyomo.Constraint(
    m.t, m.c,
    rule=thstore_max1_rule,
    doc='Thermal storage soc <= max thermal storage soc')


# -------------------------------------------------------------------------
def heatdemand_rule(m, t, c):
    return (m.q_heatdemand[t, c] 
            == data[c]["D_th"][t])
m.heatdemand = pyomo.Constraint(
    m.t, m.c, 
    rule=heatdemand_rule,
    doc='Heat demand rule')

# -------------------------------------------------------------------------
def hp_rule(m, t, c): 
    return (m.q_HPel[t, c]*data[c]["COP"][t] \
            == m.q_HPth[t, c])
m.hp = pyomo.Constraint(
    m.t, m.c,
    rule=hp_rule,
    doc='Heat pump input/output relation')

# -------------------------------------------------------------------------
def hpmax_rule(m, t, c):
    return (m.q_HPth[t, c] 
            <= data[c]["P_hp"]*deltaT)
m.hpmax = pyomo.Constraint(
    m.t, m.c, 
    rule=hpmax_rule,
    doc='Maximum heat pump heat')

# -------------------------------------------------------------------------
def hpmaxel_rule(m, t, c):
    return (m.q_HPel[t, c] 
            <= data[c]["P_hp"]*deltaT)
m.hpmaxel = pyomo.Constraint(
    m.t, m.c, 
    rule=hpmaxel_rule,
    doc='Maximum heat pump electricity')

# -------------------------------------------------------------------------
def maxdh_rule(m, t, c):
    return (m.q_dh[t, c] 
            <= data[c]["P_districtheat"]*deltaT)
m.maxdh = pyomo.Constraint(
    m.t, m.c, 
    rule=maxdh_rule,
    doc='Maximum district heat procurement power')


# -------------------------------------------------------------------------
def cooldemand_rule(m, t, c):
    return (m.q_cooldemand[t, c] 
            == data[c]["D_cool"][t])
m.cooldemand = pyomo.Constraint(
    m.t, m.c, 
    rule=cooldemand_rule,
    doc='Cooling demand rule')

# -------------------------------------------------------------------------
def elcool_rule(m, t, c): 
    return (m.q_elCoolel[t, c]*data[c]["COP_cool"][t] \
            == m.q_elCoolcool[t, c])
m.elcool = pyomo.Constraint(
    m.t, m.c,
    rule=elcool_rule,
    doc='Electric cooling input/output relation')


# -------------------------------------------------------------------------
def elcoolmax_rule(m, t, c):
    return (m.q_elCoolcool[t, c] 
            <= data[c]["P_thcool"]*deltaT)
m.elcoolmax = pyomo.Constraint(
    m.t, m.c, 
    rule=elcoolmax_rule,
    doc='Maximum electric cooling cooling')

# -------------------------------------------------------------------------
def elcoolmaxel_rule(m, t, c):
    return (m.q_elCoolel[t, c] 
            <= data[c]["P_thcool"]*deltaT)
m.elcoolmaxel = pyomo.Constraint(
    m.t, m.c, 
    rule=elcoolmaxel_rule,
    doc='Maximum electric cooling electricity')


# -------------------------------------------------------------------------
def maxdcool_rule(m, t, c):
    return (m.q_dcool[t, c] 
            <= data[c]["P_districtcool"]*deltaT)
m.maxdcool = pyomo.Constraint(
    m.t, m.c, 
    rule=maxdcool_rule,
    doc='Maximum district cooling procurement power')


# -------------------------------------------------------------------------
def emelgrid_rule(m, t, c):
    return (m.q_elgrid[t, c] *  data[c]["em_elgrid"][t]
            == m.em_elgrid[t, c])
m.emelgrid = pyomo.Constraint(
    m.t, m.c, 
    rule=emelgrid_rule,
    doc='Emissions electricity grid relation')

# -------------------------------------------------------------------------
def emdhgrid_rule(m, t, c):
    return (m.q_dh[t, c] *  data[c]["em_dhgrid"][t]
            == m.em_dh[t, c])
m.emdhgrid = pyomo.Constraint(
    m.t, m.c, 
    rule=emdhgrid_rule,
    doc='Emissions district heat grid relation')

# -------------------------------------------------------------------------
def emdcoolgrid_rule(m, t, c):
    return (m.q_dcool[t, c] *  data[c]["em_coolgrid"][t]
            == m.em_dcool[t, c])
m.emdcoolgrid = pyomo.Constraint(
    m.t, m.c, 
    rule=emdcoolgrid_rule,
    doc='Emissions district cooling grid relation')



# -------------------------------------------------------------------------
def elecbalance_rule(m, t, c):
    expr=0
    if(energy_community):
        
        expr= (m.q_elgrid[t, c] + m.q_pv[t, c] + m.q_batdischarge[t, c] - m.q_batcharge[t, c] - m.q_HPel[t, c] -  m.q_feedin[t, c] - m.q_elCoolel[t, c])
        expr -= m.q_eldemand[t, c]
        expr -= m.q_elshiftup[t,c]+m.q_elshiftdown[t,c]
        expr+=sum(m.q_c2L[t, c, j] for j in m.j if c!=j)
        expr-=sum(m.q_L2c[t, c, j] for j in m.j if c!=j)
    
        
        return expr==0
            
    else:
        expr= (m.q_elgrid[t, c] + m.q_pv[t, c] + m.q_batdischarge[t, c] - m.q_batcharge[t, c] - m.q_HPel[t, c] -  m.q_feedin[t, c] - m.q_elCoolel[t, c])
        expr -= m.q_eldemand[t, c]
        expr -= m.q_eldemand[t, c]
        expr -= m.q_elshiftup[t,c]+m.q_elshiftdown[t,c]
        return expr==0

m.elecbalance = pyomo.Constraint(
    m.t, m.c,
    rule=elecbalance_rule,
    doc='Electricity balance rule')

# -------------------------------------------------------------------------
def heatbalance_rule(m, t, c): 
    return (m.q_HPth[t, c] + m.q_dh[t, c] + m.q_thstoredischarge[t, c] - m.q_thstorecharge[t, c]  - m.q_heatshiftup[t,c] + m.q_heatshiftdown[t,c]\
            == m.q_heatdemand[t, c])
m.heatbalance = pyomo.Constraint(
    m.t, m.c,
    rule=heatbalance_rule,
    doc='Heat balance rule')

# -------------------------------------------------------------------------
def coolbalance_rule(m, t, c): 
    return (m.q_elCoolcool[t, c] + m.q_dcool[t, c]- m.q_coolshiftup[t,c] + m.q_coolshiftdown[t,c]\
            == m.q_cooldemand[t, c])
m.coolbalance = pyomo.Constraint(
    m.t, m.c,
    rule=coolbalance_rule,
    doc='Cooling balance rule')

# -------------------------------------------------------------------------
def emissionbalance_rule(m, t, c): 
    return ( m.em_elgrid[t, c] +  m.em_dh[t, c] +  m.em_dcool[t, c]\
            == m.em_total[t, c])
m.emissionbalance = pyomo.Constraint(
    m.t, m.c,
    rule=emissionbalance_rule,
    doc='Total emission calculation')

# -------------------------------------------------------------------------
# Peak power constraint
def peakpower_rule(m, t, c): 
    return (m.p_max[c] \
            >= m.q_elgrid[t, c] / deltaT)
m.peakpower = pyomo.Constraint(
    m.t, m.c, 
    rule=peakpower_rule,
    doc='Peak power rule')


# -------------------------------------------------------------------------
def compeakpowerin_rule(m, t, c, j):
    return (m.q_c2L[t, c, j] 
            <= data[c]["P_elgrid"]*deltaT)
m.compeakpowerin = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=compeakpowerin_rule,
    doc='Maximum community purchase')

# -------------------------------------------------------------------------
def compeakpowerout_rule(m, t, c, j):
    return (m.q_L2c[t, c, j] 
            <= data[c]["P_elgrid"]*deltaT)
m.compeakpowerout = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=compeakpowerout_rule,
    doc='Maximum community sale')

# -------------------------------------------------------------------------
#Community balance
def combalance_rule(m, t, c, j):
    if(energy_community):
        
        return(m.q_c2L[t, c, j] == m.q_L2c[t, j, c])
    

            
    else:
         return pyomo.Constraint.Skip

m.combalance = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=combalance_rule,
    doc='Community balance rule')

# -------------------------------------------------------------------------
#Community purchase zero
def comownpurchase_rule(m, t, c, j):
    if(energy_community):
        if(j==c):
            return(m.q_c2L[t, c, j] == 0)
        else:
            return pyomo.Constraint.Skip
    else:
        return pyomo.Constraint.Skip

m.comownpurchase = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=comownpurchase_rule,
    doc='Community own purchase prevention')

# -------------------------------------------------------------------------
#Community sale zero
def comownsale_rule(m, t, c, j):
    if(energy_community):
        if(j==c):
            return(m.q_L2c[t, c, j] == 0)
        else:
            return pyomo.Constraint.Skip
    else:
        return pyomo.Constraint.Skip

m.comownsale = pyomo.Constraint(
    m.t, m.c, m.j,
    rule=comownsale_rule,
    doc='Community own sale prevention')


# Peak power constraint aggregated
def peakpower_rule_aggregated(m, t): 
    if(energy_community and aggregate_peakpower):
        return (m.p_max_agg \
                >= sum(m.q_elgrid[t, c] / deltaT for c in m.c))
    else:
        return pyomo.Constraint.Skip
    
m.peakpower_agg = pyomo.Constraint(
    m.t,
    rule=peakpower_rule_aggregated,
    doc='Peak power rule aggregated')


# -------------------------------------------------------------------------
def maxelup_rule(m, t, c):
    return (m.q_elshiftup[t, c] 
            <= data[c]["P_elshift"][t]*deltaT)
m.maxelup = pyomo.Constraint(
    m.t, m.c, 
    rule=maxelup_rule,
    doc='Maximum electricity upshift')


def maxeldown_rule(m, t, c):
    return (m.q_elshiftdown[t, c] 
            <= data[c]["P_elshift"][t]*deltaT)
m.maxeldown = pyomo.Constraint(
    m.t, m.c, 
    rule=maxeldown_rule,
    doc='Maximum electricity downshift')


def elshiftbalance_rule(m, t, c):
    expr=0
    if(data[c]["t_elshift"]==0):
        return (m.q_elshiftup[t*data[c]["t_elshift"],c]-m.q_elshiftdown[t*data[c]["t_elshift"],c]==0)
    if((t*data[c]["t_elshift"]>len(m.timesteps)-1)):
        return pyomo.Constraint.Skip
    for i in range(0, int(data[c]["t_elshift"])):
        if((t*data[c]["t_elshift"]+i)<len(m.timesteps)):
            expr+=m.q_elshiftup[t*data[c]["t_elshift"]+i,c]-m.q_elshiftdown[t*data[c]["t_elshift"]+i,c]
    return (expr==0)
m.elshiftbalance = pyomo.Constraint(
    m.t, m.c, 
    rule=elshiftbalance_rule,
    doc='Electricity shift balance rule')



# -------------------------------------------------------------------------
def maxheatup_rule(m, t, c):
    return (m.q_heatshiftup[t, c] 
            <= data[c]["P_heatshift"][t]*deltaT)
m.maxheatup = pyomo.Constraint(
    m.t, m.c, 
    rule=maxheatup_rule,
    doc='Maximum heat upshift')


def maxheatdown_rule(m, t, c):
    return (m.q_heatshiftdown[t, c] 
            <= data[c]["P_heatshift"][t]*deltaT)
m.maxheatdown = pyomo.Constraint(
    m.t, m.c, 
    rule=maxheatdown_rule,
    doc='Maximum heat downshift')


def heatshiftbalance_rule(m, t, c):
    expr=0
    if(data[c]["t_heatshift"]==0):
        return (m.q_heatshiftup[t*data[c]["t_heatshift"],c]-m.q_heatshiftdown[t*data[c]["t_heatshift"],c]==0)
    if((t*data[c]["t_heatshift"]>len(m.timesteps)-1)):
        return pyomo.Constraint.Skip
    for i in range(0, int(data[c]["t_heatshift"])):
        if((t*data[c]["t_heatshift"]+i)<len(m.timesteps)):
            expr+=m.q_heatshiftup[t*data[c]["t_heatshift"]+i,c]-m.q_heatshiftdown[t*data[c]["t_heatshift"]+i,c]
    return (expr==0)
m.heatshiftbalance = pyomo.Constraint(
    m.t, m.c, 
    rule=heatshiftbalance_rule,
    doc='Heat shift balance rule')


# -------------------------------------------------------------------------
def maxcoolup_rule(m, t, c):
    return (m.q_coolshiftup[t, c] 
            <= data[c]["P_coolshift"][t]*deltaT)
m.maxcoolup = pyomo.Constraint(
    m.t, m.c, 
    rule=maxcoolup_rule,
    doc='Maximum cooling upshift')


def maxcooldown_rule(m, t, c):
    return (m.q_coolshiftdown[t, c] 
            <= data[c]["P_coolshift"][t]*deltaT)
m.maxcooldown = pyomo.Constraint(
    m.t, m.c, 
    rule=maxcooldown_rule,
    doc='Maximum cooling downshift')


def coolshiftbalance_rule(m, t, c):
    expr=0
    if(data[c]["t_coolshift"]==0):
        return (m.q_coolshiftup[t*data[c]["t_coolshift"],c]-m.q_coolshiftdown[t*data[c]["t_coolshift"],c]==0)
    if((t*data[c]["t_coolshift"]>len(m.timesteps)-1)):
        return pyomo.Constraint.Skip
    for i in range(0, int(data[c]["t_coolshift"])):
        if((t*data[c]["t_coolshift"]+i)<len(m.timesteps)):
            expr+=m.q_coolshiftup[t*data[c]["t_coolshift"]+i,c]-m.q_coolshiftdown[t*data[c]["t_coolshift"]+i,c]
    return (expr==0)
m.coolshiftbalance = pyomo.Constraint(
    m.t, m.c, 
    rule=coolshiftbalance_rule,
    doc='Cool shift balance rule')

# -------------------------------------------------------------------------

print("--------- Definition constraints complete -------------- \n")


# -------------------------------------------------------------------------
    # objective function
# -------------------------------------------------------------------------

def obj_costs_rule(m):
    """Calculate total costs by cost type.
    types: 'Invest', 'Fixed', 'Grid', 'Revenue'
    """       
    
    expr=      sum(m.q_elgrid[t, c]*(data[c]["C_elenergy"][t]+data[c]["C_elgrid"]) for t in m.t for c in m.c) \
                + sum((m.q_batdischarge[t, c] + m.q_batcharge[t, c])*(data[c]["C_bat"]) for t in m.t for c in m.c)  \
                + sum(m.q_HPth[t, c]*(data[c]["C_hp"]) for t in m.t for c in m.c) \
                - sum(m.q_feedin[t, c]*(data[c]["C_Feedin"][t]) for t in m.t for c in m.c)  \
                + sum(m.q_dh[t, c] * (data[c]["C_districtheat"][t]) for t in m.t for c in m.c) \
                + sum((m.q_thstoredischarge[t, c] + m.q_thstorecharge[t, c])*(data[c]["C_thstore"]) for t in m.t for c in m.c) \
                + sum(m.q_elCoolcool[t, c]*(data[c]["C_elcool"]) for t in m.t for c in m.c) \
                + sum(m.q_dcool[t, c] * (data[c]["C_districtcool"][t]) for t in m.t for c in m.c) \
                + sum(m.em_total[t, c] * (data[c]["P_CO2"][t]) for t in m.t for c in m.c) 
                
    #Minor cost increases assumed as efforts for demand shift --> to prevent increasing and decreasing in same timestep -->Can be justified as operating costs for model
    expr+= sum(m.q_elshiftup[t, c]*0.0001 for t in m.t for c in m.c)
    expr+= sum(m.q_elshiftdown[t, c]*0.0001 for t in m.t for c in m.c)
    expr+= sum(m.q_heatshiftup[t, c]*0.0001 for t in m.t for c in m.c)
    expr+= sum(m.q_heatshiftdown[t, c]*0.0001 for t in m.t for c in m.c)
    expr+= sum(m.q_coolshiftup[t, c]*0.0001 for t in m.t for c in m.c)
    expr+= sum(m.q_coolshiftdown[t, c]*0.0001 for t in m.t for c in m.c)
                
    if(energy_community):
        expr+= sum(m.q_c2L[t, c, j]*((data[c]["C_elenergy"][t]+data[c]["C_Feedin"][t])/2+data[c]["C_elgrid"]*data_EEG[c][j]) for t in m.t for c in m.c for j in m.j)
        expr-= sum(m.q_L2c[t, c, j]*((data[c]["C_elenergy"][t]+data[c]["C_Feedin"][t])/2) for t in m.t for c in m.c for j in m.j) 
        
    if(aggregate_peakpower):
        expr += m.p_max_agg*(data[consumers[0]]["C_elpower"])
    else:
        expr += sum(m.p_max[c]*(data[c]["C_elpower"]) for c in m.c)
        
        
    return expr

                
m.obj_costs = pyomo.Objective(
        rule=obj_costs_rule,
        sense=pyomo.minimize,
        doc='minimize(cost = sum of all cost)')

print("--------- Definition objective complete -------------- \n")



# -------------------------------------------------------------------------
    # Solve model
# -------------------------------------------------------------------------

solver = SolverFactory(solver)

# Solve the model
results=solver.solve(m)

print("--------- Model solved -------------- \n")

# -------------------------------------------------------------------------
    # Get Results
# -------------------------------------------------------------------------

results_timeseries=load_data.load_results(m)
results_peakpower=load_data.load_results_peakpower(m)

if(energy_community):
    results_eeg=load_data.load_results_trading(m)
    results_eeg=load_data.sort_EEG(results_eeg, consumers)
else:
    results_eeg=pd.DataFrame()
    
# -------------------------------------------------------------------------
    # Get costs
# -------------------------------------------------------------------------

costs_objective = pd.DataFrame({"Total costs" : [m.obj_costs.expr()]})
costs_timeseries = load_data.get_costs_timeseries(results_timeseries, data, consumers)
costs_power = load_data.get_costs_peakpower(results_peakpower, data, consumers, aggregate_peakpower)
if(energy_community):
    costs_eeg=load_data.get_costs_EEG(results_eeg, data, consumers, data_EEG)


# -------------------------------------------------------------------------
    # Results to excel
# -------------------------------------------------------------------------

results_excel = [results_timeseries, results_peakpower]
if energy_community:
    results_excel.append(results_eeg)
    
results_excel_costs = [costs_objective, costs_timeseries, costs_power]
if energy_community:
    results_excel_costs.append(costs_eeg)

load_data.data_to_excel(scenario, filename_out, results_excel, energy_community, results_excel_costs)
print("--------- Results written to excel file -------------- \n")


# -------------------------------------------------------------------------
    # Plot results
# -------------------------------------------------------------------------

#Electricity balance sankey for all consumers
plots.get_electricity_sankey(results_timeseries=results_timeseries, results_eeg=results_eeg, energy_community=energy_community, consumers=consumers, scenario=scenario)

#Heat balance sankey for all consumers
plots.get_heat_sankey(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Cooling balance sankey for all consumers
plots.get_cooling_sankey(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Emission balance sankey for all consumers
plots.get_emission_sankey(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Community heatmaps
if(energy_community):
   plots.get_community_heatmaps(results_eeg=results_eeg, consumers=consumers, scenario=scenario) 
   
#Peak power results
plots.get_peakpower_bar(results_peakpower=results_peakpower, aggregate_peakpower=aggregate_peakpower, scenario=scenario)

#Load curve electricity grid
plots.get_electricitygrid_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve electricity grid feedin
plots.get_electricityfeedin_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve combined
plots.get_electricitygrid_combined_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve district heat grid
plots.get_districtheat_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve district cooling grid
plots.get_districtcool_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Load curve emissions
plots.get_emission_loadcurve(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Battery timeseries
plots.get_battery_timeseries(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Thermal storage timeseries
plots.get_thstore_timeseries(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Demand shift electricity
plots.get_eldemandshift_timeseries(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Demand shift heat
plots.get_heatdemandshift_timeseries(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Demand shift cooling
plots.get_cooldemandshift_timeseries(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

#Demand shift total
plots.get_demandshift_bar(results_timeseries=results_timeseries, consumers=consumers, scenario=scenario)

# -------------------------------------------------------------------------
    # Plot costs
# -------------------------------------------------------------------------
#Peak power costs
plots.get_costs_peakpower_bar(costs_power=costs_power, scenario=scenario, consumers=consumers)


#Community costs
if(energy_community):
    plots.get_costs_community_bar(costs_eeg=costs_eeg, scenario=scenario, consumers=consumers)

#Total costs


# -------------------------------------------------------------------------
    # KPI
# -------------------------------------------------------------------------
#Plot of the KPIs and write KPI to excel --> more recommended to write KPIs in Table
plots.get_KPI(costs_objective=costs_objective, scenario=scenario, consumers=consumers, 
              results_timeseries=results_timeseries, results_peakpower=results_peakpower, results_eeg=results_eeg, 
              aggregate_peakpower=aggregate_peakpower, energy_community=energy_community)

print("--------- Plots done -------------- \n")
